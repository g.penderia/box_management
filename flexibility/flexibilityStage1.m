function [b, outputsout] = flexibilityStage1 (box, i, outputs, trialsData)
    switch box(i).Substage
    case 0 % Initialization of the stage
        box(i).RepeatCount = 5; % Initialization of the next cycle
        box(i).Substage = 1; % Determination of the next substage
        box(i).TrialStart = now;
        disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Entering stage 1')); %STATEINFO
    case 1 % Pellet distribution
        disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 1.1) : Pellet distribution', {' '}, int2str(6-box(i).RepeatCount))); %STATEINFO
        disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 1.1) : Nosepoke expected', {' '}, int2str(6-box(i).RepeatCount))); %STATEINFO
        outputs = outputmanagement(outputs, box(i).reward, 1); %Output On
        outputs = outputmanagement(outputs, box(i).reward, -1); %Output Off
        box(i).RepeatCount = box(i).RepeatCount - 1; % Count decreased
        box(i).Substage = 2; % Determination of the next substage
        box(i).TrialNumber = box(i).TrialNumber + 1;
    case 2 % Nosepoke expected
        if(box(i).nosepoke.active) % When it happens
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 1.1) : Nosepoke n�', int2str(5-box(i).RepeatCount))); %STATEINFO
            box(i).Substage = 3; % Determination of the next substage
            box(i).Tic = now; % Recording of the current date
            box(i).pelletsRetrievedStage1 = box(i).pelletsRetrievedStage1 + 1;
            fprintf(trialsData, strcat(num2str(i),',',datestr(box(i).TrialStart, 'dd-mmm-YYYY HH:MM:SS.FFF'),',',box(i).animID,',',num2str(box(i).TrialNumber),',',box(i).Stage,',','N/A',',','N/A',',','N/A',',','N/A',',',num2str(etime(datevec(now),datevec(box(i).TrialStart))),',','N/A',',','N/A',',','N/A',',','N/A','\n'));
        end
    case 3 % Cooldown
        if(box(i).RepeatCount > 0) % If the cycle needs to be repeated
            if(etime(datevec(now), datevec(box(i).Tic)) > 60) % When the cooldown is over
                box(i).Substage = 1; % Determination of the next substage : go back to 1
            end
        else % If this cycle is finished
            box(i).Substage = 4; % Determination of the next substage
            box(i).RepeatCount = 5; % Initialization of the next cycle
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Entering stage 1.2')); %STATEINFO
        end
    case 4
        if(box(i).RepeatCount > 0)
            if(box(i).nosepoke.active)
                box(i).TrialNumber = box(i).TrialNumber + 1;
                fprintf(trialsData, strcat(num2str(i),',',datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'),',',box(i).animID,',',num2str(box(i).TrialNumber),',',box(i).Stage,',','N/A',',','N/A',',','N/A',',','N/A',',','N/A',',','N/A',',','N/A',',','N/A',',','N/A','\n'));
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 1.2) : Nosepoke + Pellet distribution', {' '}, int2str(11-box(i).RepeatCount))); %STATEINFO
                outputs = outputmanagement(outputs, box(i).reward, 1); %Output On
                outputs = outputmanagement(outputs, box(i).reward, -1); %Output Off
                box(i).RepeatCount = box(i).RepeatCount - 1; % Count decreased
                box(i).pelletsRetrievedStage1 = box(i).pelletsRetrievedStage1 + 1;
                box(i).Tic = now;
                box(i).Substage = 5;
            end
        else
            box(i).EndStage1 = now;
            box(i).Stage = '2';
            box(i).Substage = 0;
        end
    case 5
        if(second(now-box(i).Tic) > 2)
            box(i).Substage = 4;
        end
    end
    b = box;
    outputsout = outputs;