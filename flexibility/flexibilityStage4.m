function [b, outputsout] = flexibilityStage4(box, i, outputs)
    switch box(i).Substage
    case 0
        box(i).Substage = 1; % Determination of the next substage
        box(i).RepeatCount = 40; % Initialization of the next cycle
        disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Entering stage 4')); %STATEINFO
        box(i).Tic = now;
    case 1
        if(second(now-box(i).Tic) > 5)
            box(i).Substage = 2;
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 4) : Nosepoke expected.')); %STATEINFO
        end
    case 2
        if(box(i).nosepoke.active)
            if(isempty(box(i).nextScreen))
                if(sum(box(i).PreviousScreensOn) > 7 || sum(box(i).PreviousScreensOn(end-3:end)) > 2)
                    box(i).activeScreen = 0;
                elseif(sum(box(i).PreviousScreensOn) < 3  || sum(box(i).PreviousScreensOn(end-3:end)) <1)
                    box(i).activeScreen = 1;
                else
                    box(i).activeScreen = randi([0,1]);
                end
            else
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 4) : Corrective trial.')); %STATEINFO
                box(i).activeScreen = box(i).nextScreen;
            end
            box(i).Substage = 3;
            if(~box(i).activeScreen)
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 4) : Nosepoke.', {' '}, 'Left screen touch expected.')); %STATEINFO
                outputs = outputmanagement(outputs, box(i).leftScreenfig1, 1); %Output On
                outputs = outputmanagement(outputs, box(i).leftScreenfig1, -1); %Output Off
            else
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 4) : Nosepoke.', {' '}, 'Right screen touch expected.')); %STATEINFO
                outputs = outputmanagement(outputs, box(i).rightScreenfig1, 1); %Output On
                outputs = outputmanagement(outputs, box(i).rightScreenfig1, -1); %Output Off
            end
            box(i).Tic = now;
        end
    case 3
        if(box(i).leftTouch.active)
            if(box(i).activeScreen)
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 4) : Right screen touched. Trial failed.')); %STATEINFO
                box(i).TrialsStage4(end+1) = 0;
                if(length(box(i).TrialsStage4) > box(i).params.nbTrialsCalc)
                    box(i).TrialsStage4 = box(i).TrialsStage4(2:end);
                end
                box(i).Substage = 4;
                outputs = outputmanagement(outputs, box(i).rightScreenfig1, 1); %Output On
                outputs = outputmanagement(outputs, box(i).rightScreenfig1, -1); %Output Off
                outputs = outputmanagement(outputs, box(i).light, 1); %Output On
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Aversive light activated.')); %STATEINFO
            else
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 4) : Left screen touched. Nosepoke expected.')); %STATEINFO
                box(i).Substage = 5;
                box(i).PreviousScreensOn(end+1) = 0;
                box(i).PreviousScreensOn = box(i).PreviousScreensOn(2:end);
                outputs = outputmanagement(outputs, box(i).blink, 1); %Output On
            end
            box(i).Tic = now;
        elseif(box(i).rightTouch.active)
            if(box(i).activeScreen)
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 4) : Right screen touched. Nosepoke expected.')); %STATEINFO
                box(i).Substage = 6;
                box(i).PreviousScreensOn(end+1) = 1;
                box(i).PreviousScreensOn = box(i).PreviousScreensOn(2:end);
                outputs = outputmanagement(outputs, box(i).blink, 1); %Output On
            else
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 4) : Left screen touched. Trial failed.')); %STATEINFO
                box(i).TrialsStage4(end+1) = 0;
                if(length(box(i).TrialsStage4) > box(i).params.nbTrialsCalc)
                    box(i).TrialsStage4 = box(i).TrialsStage4(2:end);
                end
                box(i).Substage = 4;
                outputs = outputmanagement(outputs, box(i).leftScreenfig1, 1); %Output On
                outputs = outputmanagement(outputs, box(i).leftScreenfig1, -1); %Output Off
                outputs = outputmanagement(outputs, box(i).light, 1); %Output On
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Aversive light activated.')); %STATEINFO
            end
            box(i).Tic = now;
        elseif(etime(datevec(now), datevec(box(i).Tic)) > 60)
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 4) : 60s without answer. Trial reset. ')); %STATEINFO
            if(box(i).activeScreen)
                outputs = outputmanagement(outputs, box(i).rightScreenfig1, 1); %Output On
                outputs = outputmanagement(outputs, box(i).rightScreenfig1, -1); %Output Off
            else
                outputs = outputmanagement(outputs, box(i).leftScreenfig1, 1); %Output On
                outputs = outputmanagement(outputs, box(i).leftScreenfig1, -1); %Output Off
            end
            box(i).Substage = 1;
        end
    case 4 %Trial failed
        if(second(now-box(i).Tic) > 5)
            outputs = outputmanagement(outputs, box(i).light, -1); %Output Off
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Aversive light deactivated.')); %STATEINFO
            box(i).Substage = 1;
            box(i).RepeatCount = 3;
            box(i).TrialsStage4(end+1) = 0;
            box(i).TrialsStage4 = box(i).TrialsStage4(2:end);
            box(i).nextScreen = box(i).activeScreen;
        end
    case 5
        if(second(now-box(i).Tic) > 0.75)
            outputs = outputmanagement(outputs, box(i).leftScreenfig1, 1); %Output On
            outputs = outputmanagement(outputs, box(i).leftScreenfig1, -1); %Output Off
            box(i).Tic = now;
            box(i).Substage = 7;
        end
    case 6
        if(second(now-box(i).Tic) > 0.75)
            outputs = outputmanagement(outputs, box(i).rightScreenfig1, 1); %Output On
            outputs = outputmanagement(outputs, box(i).rightScreenfig1, -1); %Output Off
            box(i).Tic = now;
            box(i).Substage = 7;
        end
    case 7 %Active screen touched
        if(second(now-box(i).Tic) > 0.75)
            box(i).Substage = 8;
            box(i).nextScreen = [];
        end
    case 8
        if(box(i).nosepoke.active)
            outputs = outputmanagement(outputs, box(i).blink, -1); %Output Off
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 4) : Nosepoke. Trial succeded : pellet distribution.')); %STATEINFO
            outputs = outputmanagement(outputs, box(i).reward, 1); %Output On
            outputs = outputmanagement(outputs, box(i).reward, -1); %Output Off
            box(i).TrialsStage4(end+1) = 1;
            box(i).TrialsStage4 = box(i).TrialsStage4(2:end);
            box(i).Substage = 1;
            if(sum(box(i).TrialsStage4)/box(i).params.nbTrialsCalc >= box(i).params.succRate)
                box(i).RepeatCount = box(i).RepeatCount - 1;
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 4) : Performance reached the plateau.')); %STATEINFO
                if(box(i).RepeatCount == 0)
                    disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 4) : Plateau maintained during', {' '}, int2str(box(i).params.nbTrialsCalc), {' '}, 'trials')); %STATEINFO
                    box(i).Stage = '5';
                    box(i).Substage = 0;
                end
            else 
                box(i).RepeatCount = 40;
            end
        elseif(second(now-box(i).Tic) > 15)
            outputs = outputmanagement(outputs, box(i).blink, -1); %Output Off
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 4) : 15s without answer. Trial reset.')); %STATEINFO
            box(i).TrialsStage4(end+1) = 0;
            box(i).TrialsStage4 = box(i).TrialsStage4(2:end);
            box(i).Substage = 1;
        end
    end
    b = box;
    outputsout = outputs;
