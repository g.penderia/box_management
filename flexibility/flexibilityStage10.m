 function [b, outputsout] = flexibilityStage10(box, i, outputs, trialsData)
    switch box(i).Substage
    case 0 % Initialization of the stage
        box(i).Substage = 1; % Determination of the next substage
        box(i).RepeatCount = 3; % Initialization of the next cycle
        disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Entering stage 10 (pilot ratio 45//55)')); %STATEINFO
        box(i).Tic = now;
    case 1
        if(second(now-box(i).Tic) > 5) % When cooldown is over
            box(i).Substage = 2;
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 10) : Nosepoke expected.')); %STATEINFO
        end
    case 2  % Awaiting nosepoke
        if(box(i).nosepoke.active) % When there is a nosepoke, a trial starts
            disp(strcat('Success rate : ', num2str(sum(box(i).TrialsStage10)/box(i).params.nbTrialsCalc)));
            box(i).TrialNumber = box(i).TrialNumber + 1;
            box(i).TrialStart = now;
            box(i).Data.stage(box(i).TrialNumber) = 6;
            box(i).Data.timestamp(box(i).TrialNumber,:) = datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF');
            if(isempty(box(i).nextScreen)) % When we do not force the next correct screen (not a corrective trial)
                box(i).Data.correctiveTrial(box(i).TrialNumber) = 0;
                box(i).correctiveTrial = 0;
                % Determination of the next correct screen
                if(sum(box(i).PreviousScreensOn) > 7 || sum(box(i).PreviousScreensOn(end-2:end))==3)
                    box(i).activeScreen = 0;
                elseif(sum(box(i).PreviousScreensOn) < 3 || sum(box(i).PreviousScreensOn(end-2:end))==0)
                    box(i).activeScreen = 1;
                else
                    box(i).activeScreen = randi([0,1]);
                end
                box(i).PreviousScreensOn = [box(i).PreviousScreensOn(2:end) box(i).activeScreen];
            else % When we force the next correct screen for a corrective trial
                box(i).correctiveTrial = 1;
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 10) : Corrective trial.')); %STATEINFO
                box(i).Data.correctiveTrial(box(i).TrialNumber) = 1;
                box(i).activeScreen = box(i).nextScreen;
            end
            box(i).Data.correctSide(box(i).TrialNumber) = box(i).activeScreen;
            box(i).Substage = 3;
            % Sending the signal to the screens
            if(mod(i,2) == 1) % We alternate the correct stimulus between two boxes to check if there is a bias
                box(i).Data.correctStim(box(i).TrialNumber,:) = 'line';
                box(i).correctStim = 45;
                if(~box(i).activeScreen)
                    disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 10) : Nosepoke.', {' '}, 'Left screen touch expected.')); %STATEINFO
                    outputs = outputmanagement(outputs, box(i).leftScreenfig1, 0.225); %Stimulus ratio max ON
                    outputs = outputmanagement(outputs, box(i).rightScreenfig1, 0.275); %Stimulus ratio min ON
                else
                    disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 10) : Nosepoke.', {' '}, 'Right screen touch expected.')); %STATEINFO
                    outputs = outputmanagement(outputs, box(i).leftScreenfig1, 0.275); %Stimulus ratio min ON
                    outputs = outputmanagement(outputs, box(i).rightScreenfig1, 0.225); %Stimulus ratio max ON
                end
            else
                box(i).Data.correctStim(box(i).TrialNumber,:) = 'cross';
                box(i).correctStim = 55;
                if(~box(i).activeScreen)
                    disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 10) : Nosepoke.', {' '}, 'Left screen touch expected.')); %STATEINFO
                    outputs = outputmanagement(outputs, box(i).leftScreenfig1, 0.275); %Stimulus ratio max ON
                    outputs = outputmanagement(outputs, box(i).rightScreenfig1, 0.225); %Stimulus ratio min ON
                else
                    disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 10) : Nosepoke.', {' '}, 'Right screen touch expected.')); %STATEINFO
                    outputs = outputmanagement(outputs, box(i).leftScreenfig1, 0.225); %Stimulus ratio min ON
                    outputs = outputmanagement(outputs, box(i).rightScreenfig1, 0.275); %Stimulus ratio max ON
                end
            end
            box(i).Tic = now;
        end
    case 3 % Awaiting response under 60 seconds
        if(box(i).leftTouch.active) % Left screen is touched
            box(i).mouseAnswer = 0;
            outputs = outputmanagement(outputs, box(i).leftScreenfig1, -1); %Stimulus OFF
            outputs = outputmanagement(outputs, box(i).rightScreenfig1, -1); %Stimulus OFF
            box(i).Data.mouseAnswer(box(i).TrialNumber) = 0;
            box(i).Data.answerTime(box(i).TrialNumber) = etime(datevec(now), datevec(box(i).Tic));
            if(box(i).activeScreen) % Wrong answer
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 10) : Left screen touched. Trial failed.')); %STATEINFO
                box(i).Data.correct(box(i).TrialNumber) = 0;
                box(i).TrialsStage10(end+1) = 0;
                box(i).TrialsStage10 = box(i).TrialsStage10(2:end);
                outputs = outputmanagement(outputs, box(i).light, 1); %Output On
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Aversive light activated.')); %STATEINFO
                box(i).Substage = 4;
            else % Right answer
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 10) : Left screen touched. Nosepoke expected.')); %STATEINFO
                box(i).Data.correct(box(i).TrialNumber) = 1;
                box(i).Substage = 5;
                box(i).PreviousScreensOn(end+1) = 1;
                box(i).PreviousScreensOn = box(i).PreviousScreensOn(2:end);
            end
            box(i).Tic = now;
        elseif(box(i).rightTouch.active) % Right screen is touched
            box(i).mouseAnswer = 1;
            outputs = outputmanagement(outputs, box(i).leftScreenfig1, -1); %Stimulus OFF
            outputs = outputmanagement(outputs, box(i).rightScreenfig1, -1); %Stimulus OFF
            box(i).Data.answerTime(box(i).TrialNumber) = etime(datevec(now), datevec(box(i).Tic));
            box(i).Data.mouseAnswer(box(i).TrialNumber) = 1;
            if(box(i).activeScreen) % Right answer
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 10) : Right screen touched. Nosepoke expected.')); %STATEINFO
                box(i).Data.correct(box(i).TrialNumber) = 1;
                box(i).Substage = 5;
                box(i).PreviousScreensOn(end+1) = 1;
                box(i).PreviousScreensOn = box(i).PreviousScreensOn(2:end);
            else % Wrong answer
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 10) : Right screen touched. Trial failed.')); %STATEINFO
                box(i).Data.correct(box(i).TrialNumber) = 0;
                box(i).TrialsStage10(end+1) = 0;
                box(i).TrialsStage10 = box(i).TrialsStage10(2:end);
                outputs = outputmanagement(outputs, box(i).light, 1); %Output On
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Aversive light activated.')); %STATEINFO
                box(i).Substage = 4;
            end
            box(i).Tic = now;
        elseif(etime(datevec(now), datevec(box(i).Tic)) > 60) % No answer after 60 seconds
            fprintf(trialsData, strcat(num2str(i),',',datestr(box(i).TrialStart, 'dd-mmm-YYYY HH:MM:SS.FFF'),',',box(i).animID,',',num2str(box(i).TrialNumber),',',box(i).Stage,',',num2str(sum(box(i).TrialsStage10)/box(i).params.nbTrialsCalc),',','0',',','0',',','60',',','15',',',num2str(box(i).correctiveTrial),',',num2str(box(i).correctStim),',',num2str(box(i).activeScreen),',','N/A','\n'));
            box(i).Data.rewardTaken(box(i).TrialNumber) = -1;
            outputs = outputmanagement(outputs, box(i).leftScreenfig1, -1); %Stimulus OFF
            outputs = outputmanagement(outputs, box(i).rightScreenfig1, -1); %Stimulus OFF
            box(i).Data.answerTime(box(i).TrialNumber) = 0;
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 10) : 60s without answer. Trial reset. ')); %STATEINFO
            box(i).Tic = now;
            box(i).Substage = 2;
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 10) : Nosepoke expected.')); %STATEINFO
        end
    case 4 %Trial failed
        if(second(now-box(i).Tic) > 5)
            fprintf(trialsData, strcat(num2str(i),',',datestr(box(i).TrialStart, 'dd-mmm-YYYY HH:MM:SS.FFF'),',',box(i).animID,',',num2str(box(i).TrialNumber),',',box(i).Stage,',',num2str(sum(box(i).TrialsStage10)/box(i).params.nbTrialsCalc),',','0',',','-1',',',num2str(etime(datevec(box(i).Tic),datevec(box(i).TrialStart))),',','15',',',num2str(box(i).correctiveTrial),',',num2str(box(i).correctStim),',',num2str(box(i).activeScreen),',',num2str(abs(box(i).activeScreen - 1)),'\n'));
            box(i).Data.rewardTaken(box(i).TrialNumber) = -2;
            outputs = outputmanagement(outputs, box(i).light, -1); %Output Off
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Aversive light deactivated.')); %STATEINFO
            box(i).Substage = 1;
            box(i).nextScreen = box(i).activeScreen;
        end
    case 5 % Correct screen touched
            outputs = outputmanagement(outputs, box(i).blink, 1); %Output On
            box(i).nextScreen = [];
            box(i).Substage = 6;
    case 6 % Nosepoke expected within 15s
        if(etime(datevec(now), datevec(box(i).Tic))<15)
            if(box(i).nosepoke.active)
                fprintf(trialsData, strcat(num2str(i),',',datestr(box(i).TrialStart, 'dd-mmm-YYYY HH:MM:SS.FFF'),',',box(i).animID,',',num2str(box(i).TrialNumber),',',box(i).Stage,',',num2str(sum(box(i).TrialsStage10)/box(i).params.nbTrialsCalc),',','1',',','1',',',num2str(etime(datevec(box(i).Tic),datevec(box(i).TrialStart))),',',num2str(etime(datevec(now),datevec(box(i).Tic))),',',num2str(box(i).correctiveTrial),',',num2str(box(i).correctStim),',',num2str(box(i).activeScreen),',',num2str(box(i).mouseAnswer),'\n'));
                box(i).Data.rewardTaken(box(i).TrialNumber) = etime(datevec(now), datevec(box(i).Tic));
                outputs = outputmanagement(outputs, box(i).blink, -1); %Output Off
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 5) : Nosepoke. Trial succeded : pellet distribution.')); %STATEINFO
                outputs = outputmanagement(outputs, box(i).reward, 1); %Output On
                outputs = outputmanagement(outputs, box(i).reward, -1); %Output Off
                box(i).TrialsStage10(end+1) = 1;
                box(i).TrialsStage10 = box(i).TrialsStage10(2:end);
                box(i).Substage = 1;
                box(i).Tic = now;
                if(sum(box(i).TrialsStage10)/box(i).params.nbTrialsCalc >= box(i).params.succRate)
                    %box(i).RepeatCount = box(i).RepeatCount - 1;
                    %if(box(i).RepeatCount == 0)
                        box(i).Stage = '6';
                        box(i).Substage = 0;
                    %end
                end
            end
        else
            box(i).Substage = 7;
            outputs = outputmanagement(outputs, box(i).blink, -1); %Output Off
        end
    case 7
        if(box(i).nosepoke.active)
            fprintf(trialsData, strcat(num2str(i),',',datestr(box(i).TrialStart, 'dd-mmm-YYYY HH:MM:SS.FFF'),',',box(i).animID,',',num2str(box(i).TrialNumber),',',box(i).Stage,',',num2str(sum(box(i).TrialsStage10)/box(i).params.nbTrialsCalc),',','0',',','-2',',',num2str(etime(datevec(box(i).Tic),datevec(box(i).TrialStart))),',',num2str(etime(datevec(now),datevec(box(i).Tic))),',',num2str(box(i).correctiveTrial),',',num2str(box(i).correctStim),',',num2str(box(i).activeScreen),',',num2str(box(i).mouseAnswer),'\n'));
            box(i).Data.rewardTaken(box(i).TrialNumber) = etime(datevec(now), datevec(box(i).Tic));
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 5) : 15s without answer. Trial reset.')); %STATEINFO
            box(i).TrialsStage10(end+1) = 0;
            box(i).TrialsStage10 = box(i).TrialsStage10(2:end);
            box(i).Substage = 1;
        end
    end
    b = box;
    outputsout = outputs;
end