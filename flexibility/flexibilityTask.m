function [b, outputsout] = flexibilityTask(box, i, outputs, trialsData)

    switch box(i).Stage

        case 'Start'
            outputs = outputmanagement(outputs, box(i).rightScreenfig1, 2);
            outputs = outputmanagement(outputs, box(i).leftScreenfig1, 2);
            for z = 1:50 % boucle qui ralentit tr�s l�g�rement le programme afin que les signaux soient distinctibles
                x=now-z;
            end
            outputs = outputmanagement(outputs, box(i).rightScreenfig1, -1);
            outputs = outputmanagement(outputs, box(i).leftScreenfig1, -1);
            box(i).StartTime = now;
            box(i).Stage = '6';
            box(i).Substage = 0;
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Start.')); %STATEINFO
        case '1' 
            [box, outputs] = flexibilityStage1(box, i, outputs, trialsData);
            %box(i).Stage = '5';    %Eric added that to start at stage 5 directly...
        case '2' 
            [box, outputs] = flexibilityStage2(box, i, outputs, trialsData);
        case '3'
            [box, outputs] = flexibilityStage3(box, i, outputs, trialsData);
        case '4'
            % stage skipped
            %[box, outputs] = flexibilityStage4(box, i, outputs);
            box(i).Stage = '5';
        case '5'
            [box, outputs] = flexibilityStage5(box, i, outputs, trialsData);
        case '6'
            [box, outputs] = flexibilityStage6(box, i, outputs, trialsData);
        case '7'
            [box, outputs] = flexibilityStage7(box, i, outputs, trialsData);
        case '8'
            [box, outputs] = flexibilityStage8(box, i, outputs, trialsData);
        case '9'
            [box, outputs] = flexibilityStage9(box, i, outputs, trialsData);
        case '10'
            [box, outputs] = flexibilityStage10(box, i, outputs, trialsData);
        case 'Finish'
            if(etime(datevec(now), datevec(box(i).Tic)) > 1800) % pellets toutes les x secondes (ici x = 1800)
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Post-end : Pellet distribution', {' '}, int2str(6-box(i).RepeatCount))); %STATEINFO
                outputs = outputmanagement(outputs, box(i).reward, 1); %Output On
                outputs = outputmanagement(outputs, box(i).reward, -1); %Output Off
                box(i).PelletsRetrieved = box(i).PelletsRetrieved + 1;
                box(i).Tic = now;
            end
    end
    outputsout = outputs;
    b = box;
end