function [b, outputsout] = switchStage2(box, i, outputs)
    switch box(i).Substage
    case 0
        box(i).Substage = 1; % Determination of the next substage
        box(i).RepeatCount = 10; % Initialization of the next cycle
        disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Entering stage 2')); %STATEINFO
        disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 2) : Screen touch expected.', {' '}, int2str(box(i).RepeatCount), ' remaining. ')); %STATEINFO
    case 1
        if(box(i).leftTouch.active || box(i).rightTouch.active)
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 2) : Screen touched', {' '}, int2str(11-box(i).RepeatCount))); %STATEINFO
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 2) : Pellet distribution', {' '}, int2str(11-box(i).RepeatCount))); %STATEINFO
            outputs = outputmanagement(outputs, box(i).reward, 1); %Output On
            outputs = outputmanagement(outputs, box(i).reward, -1); %Output Off
            box(i).Substage = 2;
            box(i).Tic = now;
            outputs = outputmanagement(outputs, box(i).blink, 1); %Output On
        end
    case 2
        if(etime(datevec(now), datevec(box(i).Tic)) > 15)
            outputs = outputmanagement(outputs, box(i).blink, -1); %Output Off
        end                
        if(box(i).nosepoke.active)
            outputs = outputmanagement(outputs, box(i).blink, -1); %Output Off
            if(etime(datevec(now), datevec(box(i).Tic)) < 15)
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 2) : Pellet retrieved within 15s.')); %STATEINFO
                box(i).RepeatCount = box(i).RepeatCount - 1;
                if(box(i).RepeatCount == 0)
                    box(i).Stage = '3';
                    box(i).Substage = 0;
                else
                    box(i).Substage = 1;
                end
            else
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 2) : Pellet retrieved after 15s.')); %STATEINFO
                box(i).RepeatCount = 10;
                box(i).Substage = 1;
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 2) : Nosepoke expected.')); %STATEINFO
            end
            box(i).pelletsRetrievedStage2 = box(i).pelletsRetrievedStage2 + 1;
        end
    end
    b = box;
    outputsout = outputs;