%Marine EUVRARD. 8/11/2016
% tirage des stimuli visuels en fonction de la r�gle tir�e initialement. La
% target �tant facilement d�terminable (seul 0 ou seul 1), on peut ainsi
% d�finir son num�ro de colonnes (et donc les stimuli) et en fonction des
% diff�rents cas, on peut conna�tre les num�ros de colonnes (et donc la
% nature des stimuli visuels) des non-target. Ensuite, on peut tirer au
% hasard les diff�rents stimuli visuels en imposant une fr�quence de
% pr�sentation (50% du temps pour la target, 25% du temps pour chaque
% non-target)

function regle = tirageStimulusNEW(tableau, previous)
    % d�finition de la target
    for i = 1:3
        if tableau(i) == 1 && sum(tableau) == 1
            target = i;
        elseif tableau(i) == 0 && sum(tableau) == 2
            target = i;
        end
    end
    % d�finition des non-target
    if target == 1
        nontarget1 = 2;
        nontarget2 = 3;
    elseif target == 2
        nontarget1 = 1;
        nontarget2 = 3;
    elseif target == 3
        nontarget1 = 1;
        nontarget2 = 2;
    end
    % affichage � l'�cran des formes en fonction des fr�quences
    if sum(previous == target) <= 8
        regle = target;
    elseif sum(previous == nontarget1) <= 4
        regle = nontarget1;
    elseif sum(previous == nontarget2) <= 4
        regle = nontarget2;
    else
        if sum(previous == target) >= 12
        m = [3,4];
        elseif sum(previous == nontarget1) >= 6
            m = [1,2,4];
        elseif sum(previous == nontarget2) >= 6
            m = [1,2,3];
        else
            m = [1,2,3,4];
        end
        aleatoire = randi([1,length(m)]);
         if m(aleatoire) == 1 || m(aleatoire) == 2
            disp(target)
            regle = target;
        elseif m(aleatoire) == 3
            disp(nontarget1)
            regle = nontarget1;
        else
            disp(nontarget2)
            regle = nontarget2;
        end
    end
end

    
        
    