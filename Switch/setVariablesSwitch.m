function varargout = setVariablesSwitch(varargin)
% SETVARIABLESSWITCH MATLAB code for setVariablesSwitch.fig
%      SETVARIABLESSWITCH, by itself, creates a new SETVARIABLESSWITCH or raises the existing
%      singleton*.
%
%      H = SETVARIABLESSWITCH returns the handle to a new SETVARIABLESSWITCH or the handle to
%      the existing singleton*.
%
%      SETVARIABLESSWITCH('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SETVARIABLESSWITCH.M with the given input arguments.
%
%      SETVARIABLESSWITCH('Property','Value',...) creates a new SETVARIABLESSWITCH or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before setVariablesSwitch_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to setVariablesSwitch_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help setVariablesSwitch

% Last Modified by GUIDE v2.5 11-Jul-2016 11:15:27

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @setVariablesSwitch_OpeningFcn, ...
                   'gui_OutputFcn',  @setVariablesSwitch_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before setVariablesSwitch is made visible.
function setVariablesSwitch_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to setVariablesSwitch (see VARARGIN)

% Choose default command line output for setVariablesSwitch
handles.ok = 0;
handles.varargin = varargin{1};
if(isstruct(varargin{1}))
    set(handles.succRate, 'String', sprintf('%g', sscanf(num2str(varargin{1}.succRate), '%g', 1)));
    set(handles.nbTrialsCalc, 'String', sprintf('%g', sscanf(num2str(varargin{1}.nbTrialsCalc), '%g', 1)));
    set(handles.nbTrialsPlot, 'String', sprintf('%g', sscanf(num2str(varargin{1}.nbTrialsPlot), '%g', 1)));
end
% Update handles structure
guidata(hObject, handles);
movegui(handles.setVariablesSwitchWindow, 'center');
warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
javaFrame = get(hObject,'JavaFrame');
javaFrame.setFigureIcon(javax.swing.ImageIcon('icon.png'));
% UIWAIT makes setVariablesSwitch wait for user response (see UIRESUME)
uiwait(handles.setVariablesSwitchWindow);


% --- Outputs from this function are returned to the command line.
function varargout = setVariablesSwitch_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
if(handles.ok)
    varargout{1} = handles.params;
else
    varargout{1} = handles.varargin;
end
delete(handles.setVariablesSwitchWindow);


function setVariablesSwitch_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to launcher (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: delete(hObject) closes the figure
close=questdlg('Close without saving ?','Confirm your action','Yes','No','No');
if(strcmp(close,'Yes'))
    uiresume(handles.setVariablesSwitchWindow);
end


% --- Executes on button press in ok_button.
function ok_button_Callback(hObject, eventdata, handles)
% hObject    handle to ok_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.ok = 1;
handles.params.succRate = str2double(get(handles.succRate, 'string'));
handles.params.nbTrialsCalc = str2double(get(handles.nbTrialsCalc, 'string'));
handles.params.nbTrialsPlot = str2double(get(handles.nbTrialsPlot, 'string'));
guidata(handles.setVariablesSwitchWindow, handles);
uiresume(handles.setVariablesSwitchWindow);


function checkContent(hObject)
S = get(hObject, 'String');
if ~all(ismember(S, '-0123456789.')) || strcmp(S,'-')
    warndlg('This field can only contain integers.','Error');
    if(~isempty(S))
        set(hObject, 'String', num2str(S(1)));
    else
        set(hObject, 'String', num2str(0));
    end
elseif (isempty(S))
    set(hObject, 'String', num2str(0));
end


function succRate_Callback(hObject, eventdata, handles)
% hObject    handle to succRate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of succRate as text
%        str2double(get(hObject,'String')) returns contents of succRate as a double
checkContent(hObject);

% --- Executes during object creation, after setting all properties.
function succRate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to succRate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function nbTrialsCalc_Callback(hObject, eventdata, handles)
% hObject    handle to nbTrialsCalc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nbTrialsCalc as text
%        str2double(get(hObject,'String')) returns contents of nbTrialsCalc as a double

checkContent(hObject);
S = int2str(round(str2double(get(hObject, 'String'))));
set(hObject, 'String', sprintf('%g', sscanf(S, '%g', 1)));

% --- Executes during object creation, after setting all properties.
function nbTrialsCalc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nbTrialsCalc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function nbTrialsPlot_Callback(hObject, eventdata, handles)
% hObject    handle to nbTrialsPlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nbTrialsPlot as text
%        str2double(get(hObject,'String')) returns contents of nbTrialsPlot as a double
checkContent(hObject);
S = int2str(round(str2double(get(hObject, 'String'))));
set(hObject, 'String', sprintf('%g', sscanf(S, '%g', 1)));

% --- Executes during object creation, after setting all properties.
function nbTrialsPlot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nbTrialsPlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
