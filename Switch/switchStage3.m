function [b, outputsout] = switchStage3(box, i, outputs)
    switch box(i).Substage
    case 0
        box(i).Substage = 1; % Determination of the next substage
        box(i).RepeatCount = 10; % Initialization of the next cycle
        disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Entering stage 3')); %STATEINFO
        box(i).Tic = now;
    case 1
        if(second(now-box(i).Tic) > 5)
            box(i).Substage = 2;
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 3) : Nosepoke expected.', {' '}, int2str(box(i).RepeatCount), ' remaining. ')); %STATEINFO
        end
    case 2
        if(box(i).nosepoke.active)
            outputs = outputmanagement(outputs, box(i).leftScreenfig2, 1); %Output On
            outputs = outputmanagement(outputs, box(i).rightScreenfig2, 1); %Output On
            box(i).Substage = 3;
            box(i).Tic = now;
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 3) : Screen touch expected.', {' '}, int2str(box(i).RepeatCount), ' remaining. ')); %STATEINFO
        end
    case 3
        if(box(i).leftTouch.active || box(i).rightTouch.active)
            outputs = outputmanagement(outputs, box(i).leftScreenfig2, -1); %Output Off
            outputs = outputmanagement(outputs, box(i).rightScreenfig2, -1); %Output Off
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 3) : Screen touch.', {' '}, int2str(box(i).RepeatCount), ' remaining. ')); %STATEINFO
            box(i).Substage = 4;
            outputs = outputmanagement(outputs, box(i).blink, 1); %Output On
            box(i).Tic=now;
        elseif(etime(datevec(now), datevec(box(i).Tic)) > 60)
            outputs = outputmanagement(outputs, box(i).leftScreenfig2, -1); %Output Off
            outputs = outputmanagement(outputs, box(i).rightScreenfig2, -1); %Output Off
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 3) : No screen touch within 60s.')); %STATEINFO
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 3) : Nosepoke expected.', {' '}, int2str(box(i).RepeatCount), ' remaining. ')); %STATEINFO
            box(i).Substage = 2;
        end
    case 4
        if(box(i).nosepoke.active)
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 3) : Nosepoke.', {' '}, int2str(box(i).RepeatCount), ' remaining. ')); %STATEINFO
            outputs = outputmanagement(outputs, box(i).blink, -1); %Output Off
            outputs = outputmanagement(outputs, box(i).reward, 1); %Output On
            outputs = outputmanagement(outputs, box(i).reward, -1); %Output Off
            box(i).pelletsRetrievedStage3 = box(i).pelletsRetrievedStage3 + 1;
            box(i).RepeatCount = box(i).RepeatCount - 1;
            if(box(i).RepeatCount == 0)
                box(i).Stage = '5';
                box(i).Substage = 0;
            else
                box(i).Substage = 1;
            end
        elseif(second(now-box(i).Tic) > 15)
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 3) : No nosepoke within 15s.')); %STATEINFO
            outputs = outputmanagement(outputs, box(i).blink, -1); %Output Off
            box(i).RepeatCount = 10;
            box(i).Substage = 5;
        end
    case 5
        if(box(i).nosepoke.active)
            outputs = outputmanagement(outputs, box(i).reward, 1); %Output On
            outputs = outputmanagement(outputs, box(i).reward, -1); %Output Off
            box(i).Substage = 1;
        end
    end
    b = box;
    outputsout = outputs;