function [b, outputsout] = switchTask(box, i, outputs)

    switch box(i).Stage

        case 'Start'
            outputs = outputmanagement(outputs, box(i).rightScreenfig1, 3);
            outputs = outputmanagement(outputs, box(i).leftScreenfig1, 3);
            for z = 1:30 % boucle qui ralentit tr�s l�g�rement le programme afin que les signaux soient distinctibles
                x=now-z;
            end
            outputs = outputmanagement(outputs, box(i).rightScreenfig1, -1);
            outputs = outputmanagement(outputs, box(i).leftScreenfig1, -1);
            box(i).StartTime = now;
            box(i).Stage = '5';
            box(i).Substage = 0;
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Start.')); %STATEINFO
        case '1' 
            [box, outputs] = switchStage1(box, i, outputs);
        case '2' 
            [box, outputs] = switchStage2(box, i, outputs);
        case '3'
            [box, outputs] = switchStage3(box, i, outputs);
        case '4'
            % stage skipped
            %[box, outputs] = switchStage4(box, i, outputs);
            box(i).Stage = '5';
        case '5'
            [box, outputs] = switchStage5(box, i, outputs);    
        case 'Finish'
            if(etime(datevec(now), datevec(box(i).Tic)) > 1800) % pellets toutes les x secondes (ici x = 1800)
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Post-end : Pellet distribution', {' '}, int2str(6-box(i).RepeatCount))); %STATEINFO
                outputs = outputmanagement(outputs, box(i).reward, 1); %Output On
                outputs = outputmanagement(outputs, box(i).reward, -1); %Output Off
                box(i).PelletsRetrieved = box(i).PelletsRetrieved + 1;
                box(i).Tic = now;
            end
    end
    outputsout = outputs;
    b = box;
end