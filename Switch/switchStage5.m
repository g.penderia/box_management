function [b, outputsout] = switchStage5(box, i, outputs)
switch box(i).Substage
    case 0 % Initialization
        box(i).Substage = 1; %Next substage
        box(i).z = 0 ;
        disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Entering stage 5 (pilot)')); %STATEINFO
        box(i).Tic = now;
        box(i).regle = tableau();
        box(i).regleinterdite(1) = bi2de(box(i).regle);
        box(i).Stim1 = 0;
        box(i).Stim2 = 0;
        box(i).Stim3 = 0;
        box(i).TrialNumber = 0;
        box(i).BlockNumber = 0;
        for n = 1:3  % difinition de la target
            if box(i).regle(n) == 1 && sum(box(i).regle) == 1
                box(i).t = n;
            elseif box(i).regle(n) == 0 && sum(box(i).regle) == 2
            end
            box(i).t = n;
        end
        % difinition des non-target
        box(i).Data.Stim = [];
        box(i).Data.answerTime = [];
        box(i).Stim = [];
        box(i).Correct1 = [];
        box(i).Correct2 = [];
        box(i).Correct3 = [];
        if box(i).t == 1
            box(i).nt1 = 2;
            box(i).nt2 = 3;
        elseif box(i).t == 2
            box(i).nt1 = 1;
            box(i).nt2 = 3;
        elseif box(i).t == 3
            box(i).nt1 = 1;
            box(i).nt2 = 2;
        end
        disp(box(i).regle)
        box(i).RepeatCount = 6; %ou 4
    case 1
        if(second(now-box(i).Tic) > 5) % When cooldown is over
            box(i).Substage = 2;
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 5) : Nosepoke expected.')); %STATEINFO
        end
    case 2  % Awaiting nosepoke
        if(box(i).nosepoke.active) % When there is a nosepoke, a trial starts
            box(i).BlockNumber = box(i).BlockNumber + 1;
            box(i).TrialNumber = box(i).TrialNumber + 1;
            box(i).Data.TrialNumber(box(i).TrialNumber) = box(i).TrialNumber;
            box(i).Data.timestamp(box(i).TrialNumber,:) = datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF');
            box(i).Data.time(box(i).TrialNumber) = now;
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Trial n�', num2str(box(i).TrialNumber))); %STATEINFO
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Success rate :', {' '}, num2str(sum(box(i).TrialsStage5)/box(i).params.nbTrialsCalc))); %STATEINFO
            if(isempty(box(i).nextStim)) % When we do not force the next correct screen (not a corrective trial)
                box(i).z=box(i).z+1;
                box(i).Data.correctiveTrial(box(i).TrialNumber) = 0;
                box(i).Stim(box(i).z) = randi([1,3]);
                box(i).Data.Stim(box(i).TrialNumber)=box(i).Stim(box(i).z);
                if box(i).z <= 20
                    if sum(box(i).Stim(:) == box(i).t)/box(i).z >= 0.6
                        if sum(box(i).Stim(:) == box(i).nt1)/box(i).z > 0.25
                            box(i).Stim(box(i).z) = box(i).nt2;
                            box(i).Data.Stim(box(i).TrialNumber)=box(i).nt2;
                        else
                            box(i).Stim(box(i).z) = box(i).nt1;
                            box(i).Data.Stim(box(i).TrialNumber)=box(i).nt1;
                        end
                    elseif sum(box(i).Stim(:) == box(i).t)/box(i).z <= 0.4
                        box(i).Stim(box(i).z) = box(i).t;
                        box(i).Data.Stim(box(i).TrialNumber)=box(i).t;
                    end
                else
                    if sum(box(i).Stim(box(i).z-19:box(i).z) == box(i).t) > 10
                        if sum(box(i).Stim(box(i).z-19:box(i).z) == box(i).nt1) > 5
                            box(i).Stim(box(i).z)=box(i).nt2;
                            box(i).Data.Stim(box(i).TrialNumber) = box(i).nt2;
                        else
                            box(i).Data.Stim(box(i).TrialNumber) = box(i).nt1;
                            box(i).Stim(box(i).z)=box(i).nt1;
                        end
                    elseif sum(box(i).Stim(box(i).z-19:box(i).z) == box(i).t) < 10
                        box(i).Data.Stim(box(i).TrialNumber) = box(i).t;
                        box(i).Stim(box(i).z) = box(i).t;
                    end
                end
            else % When we force the next correct screen for a corrective trial
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 5) : Corrective trial.')); %STATEINFO
                box(i).Data.correctiveTrial(box(i).TrialNumber) = 1;
                box(i).Data.Stim(box(i).TrialNumber) = box(i).nextStim;
            end
            
            box(i).Data.regle(box(i).TrialNumber,:)= box(i).regle;
            box(i).Substage = 3;
            disp(box(i).Data.Stim(box(i).TrialNumber))
            outputs = outputmanagement(outputs, box(i).rightScreenfig1, box(i).Data.Stim(box(i).TrialNumber));
            outputs = outputmanagement(outputs, box(i).leftScreenfig1, box(i).Data.Stim(box(i).TrialNumber));
            box(i).Tic = now;
            box(i).TargetScreen = box(i).regle(box(i).Data.Stim(box(i).TrialNumber));
            box(i).Data.TargetScreen(box(i).TrialNumber) = box(i).TargetScreen;
        end
    case 3 % Awaiting response under 60 seconds
        if(box(i).leftTouch.active) % Left screen is touched
            switch box(i).Data.Stim(box(i).TrialNumber)
                case 1
                    box(i).Stim1 = box(i).Stim1 +1;
                case 2
                    box(i).Stim2 = box(i).Stim2 +1;
                case 3
                    box(i).Stim3 = box(i).Stim3 +1;
            end
            box(i).Data.mouseAnswer(box(i).TrialNumber) = 0;
            box(i).Data.answerTime(box(i).TrialNumber) = etime(datevec(now), datevec(box(i).Tic));
            if box(i).TargetScreen == 0 % Right answer
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 5) : Left screen touched. Nosepoke expected.')); %STATEINFO
                box(i).Data.correct(box(i).TrialNumber) = 1;
                switch box(i).Data.Stim(box(i).TrialNumber)
                    case 1
                        box(i).Correct1(box(i).Stim1)=1;
                    case 2
                        box(i).Correct2(box(i).Stim2)=1;
                    case 3
                        box(i).Correct3(box(i).Stim3)=1;
                end
                box(i).Substage = 5;
            else % Wrong answer
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 5) : Left screen touched. Trial failed.')); %STATEINFO
                box(i).Data.correct(box(i).TrialNumber) = 0;
                switch box(i).Data.Stim(box(i).TrialNumber)
                    case 1
                        box(i).Correct1(box(i).Stim1)=0;
                    case 2
                        box(i).Correct2(box(i).Stim2)=0;
                    case 3
                        box(i).Correct3(box(i).Stim3)=0;
                end
                box(i).TrialsStage5(end+1) = 0;
                box(i).TrialsStage5 = box(i).TrialsStage5(2:end);
                outputs = outputmanagement(outputs, box(i).light, 1); %Output On
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Aversive light activated.')); %STATEINFO
                box(i).Tic = now;
                box(i).Substage = 4;
            end
            box(i).Tic = now;
            outputs = outputmanagement(outputs, box(i).rightScreenfig1, -1);
            outputs = outputmanagement(outputs, box(i).leftScreenfig1, -1);
        elseif (box(i).rightTouch.active) % Right screen is touched
            switch box(i).Data.Stim(box(i).TrialNumber)
                case 1
                    box(i).Stim1 = box(i).Stim1 +1;
                case 2
                    box(i).Stim2 = box(i).Stim2 +1;
                case 3
                    box(i).Stim3 = box(i).Stim3 +1;
            end
            box(i).Data.answerTime(box(i).TrialNumber) = etime(datevec(now), datevec(box(i).Tic));
            box(i).Data.mouseAnswer(box(i).TrialNumber) = 1;
            if box(i).TargetScreen == 1 % Correct answer
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 5) : Right screen touched. Nosepoke expected.')); %STATEINFO
                box(i).Data.correct(box(i).TrialNumber) = 1;
                switch box(i).Data.Stim(box(i).TrialNumber)
                    case 1
                        box(i).Correct1(box(i).Stim1)=1;
                    case 2
                        box(i).Correct2(box(i).Stim2)=1;
                    case 3
                        box(i).Correct3(box(i).Stim3)=1;
                end
                box(i).Substage = 5;
            else % Wrong answer
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 5) : Right screen touched. Trial failed.')); %STATEINFO
                box(i).Data.correct(box(i).TrialNumber) = 0;
                switch box(i).Data.Stim(box(i).TrialNumber)
                    case 1
                        box(i).Correct1(box(i).Stim1)=0;
                    case 2
                        box(i).Correct2(box(i).Stim2)=0;
                    case 3
                        box(i).Correct3(box(i).Stim3)=0;
                end
                box(i).TrialsStage5(end+1) = 0;
                box(i).TrialsStage5 = box(i).TrialsStage5(2:end);
                outputs = outputmanagement(outputs, box(i).light, 1); %Output On
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Aversive light activated.')); %STATEINFO
                box(i).Tic = now;
                box(i).Substage = 4;
            end
            box(i).Tic = now;
            outputs = outputmanagement(outputs, box(i).rightScreenfig1, -1);
            outputs = outputmanagement(outputs, box(i).leftScreenfig1, -1);
        elseif (etime(datevec(now), datevec(box(i).Tic)) > 60) % No answer after 60 seconds
            box(i).Data.answerTime(box(i).TrialNumber) = 0;
            box(i).BlockNumber = box(i).BlockNumber - 1;
            box(i).TrialNumber = box(i).TrialNumber - 1;
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 5) : 60s without answer. Trial reset. ')); %STATEINFO
            box(i).Tic = now;
            outputs = outputmanagement(outputs, box(i).rightScreenfig1, -1);
            outputs = outputmanagement(outputs, box(i).leftScreenfig1, -1);
            box(i).Substage = 2;
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 5) : Nosepoke expected.')); %STATEINFO
        end
    case 4 % Trial failed
        if(second(now-box(i).Tic) > 5)
            outputs = outputmanagement(outputs, box(i).light, -1); %Output Off
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Aversive light deactivated.')); %STATEINFO
            box(i).Substage = 1;
            box(i).nextStim = box(i).Data.Stim(box(i).TrialNumber);
        end
    case 5 % Correct screen touched
        outputs = outputmanagement(outputs, box(i).blink, 1); %Output On
        box(i).Substage = 6;
        box(i).nextStim = [];
    case 6 % Nosepoke expected within 15s
        if(box(i).nosepoke.active)
            box(i).Data.rewardTaken(box(i).TrialNumber) = etime(datevec(now), datevec(box(i).Tic));
            outputs = outputmanagement(outputs, box(i).blink, -1); %Output Off
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 5) : Nosepoke. Trial succeded : pellet distribution.')); %STATEINFO
            outputs = outputmanagement(outputs, box(i).reward, 1); %Output On
            outputs = outputmanagement(outputs, box(i).reward, -1); %Output Off
            box(i).TrialsStage5(end+1) = 1;
            box(i).TrialsStage5 = box(i).TrialsStage5(2:end);
            box(i).Substage = 1;
%             switch mod(i,2) 
%                 case 1
                    if(sum(box(i).TrialsStage5)/box(i).params.nbTrialsCalc >= box(i).params.succRate) && box(i).BlockNumber > 39
                        box(i).RepeatCount = box(i).RepeatCount - 1;
                        box(i).BlockNumber = 0;
                        box(i).z = 0;
                        if(box(i).RepeatCount == 0)
                            box(i).Stage = 'Finish';
                        else
                            box(i).Substage = 7;
                        end
                    end
%                 case 0
%                     if box(i).Stim1 > 39 && box(i).Stim2 > 39 && box(i).Stim3 > 39
%                     if sum(box(i).Correct1(box(i).Stim1-39:box(i).Stim1))/box(i).params.nbTrialsCalc >= box(i).params.succRate ...
%                             && sum(box(i).Correct2(box(i).Stim2-39:box(i).Stim2))/box(i).params.nbTrialsCalc >= box(i).params.succRate ...
%                             && sum(box(i).Correct3(box(i).Stim3-39:box(i).Stim3))/box(i).params.nbTrialsCalc >= box(i).params.succRate
%                         box(i).RepeatCount = box(i).RepeatCount - 1;
%                         box(i).BlockNumber = 0;
%                         box(i).z = 0;
%                         box(i).Stim1 = 0;
%                         box(i).Stim2 = 0;
%                         box(i).Stim3 = 0;
%                         if(box(i).RepeatCount == 0)
%                             box(i).Stage = 'Finish';
%                         else
%                             box(i).Substage = 7;
%                         end
%                     end
%                     end
%             end
        elseif(second(now-box(i).Tic) > 15)
            box(i).Data.rewardTaken(box(i).TrialNumber) = 0;
            outputs = outputmanagement(outputs, box(i).blink, -1); %Output Off
            disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - (Stage 5) : 15s without answer. Trial reset.')); %STATEINFO
            box(i).TrialsStage5(end+1) = 0;
            switch box(i).Data.Stim(box(i).TrialNumber)
                case 1
                    box(i).Correct1(box(i).Stim1)=0;
                case 2
                    box(i).Correct2(box(i).Stim2)=0;
                case 3
                    box(i).Correct3(box(i).Stim3)=0;
            end
            box(i).TrialsStage5 = box(i).TrialsStage5(2:end);
            box(i).Substage = 1;
        end
    case 7 % tirage d'une nouvelle r�gle
        box(i).Substage = 1; %Next substage
        disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - New rule')); %STATEINFO
        box(i).Tic = now;
        box(i).regle= tableaunew3(box(i).regle);
        binaire = bi2de(box(i).regle);
        while any(box(i).regleinterdite==binaire)
            box(i).regle= tableaunew3(box(i).regle);
        end
        box(i).regleinterdite(end+1) = bi2de(box(i).regle);
        for n = 1:3  % difinition de la target
            if box(i).regle(n) == 1 && sum(box(i).regle) == 1
                box(i).t = n;
            elseif box(i).regle(n) == 0 && sum(box(i).regle) == 2
                box(i).t = n;
            end
        end
        % d�finition des non-target
        if box(i).t == 1
            box(i).nt1 = 2;
            box(i).nt2 = 3;
        elseif box(i).t == 2
            box(i).nt1 = 1;
            box(i).nt2 = 3;
        elseif box(i).t == 3
            box(i).nt1 = 1;
            box(i).nt2 = 2;
        end
        disp(box(i).regle)
end
b = box;
outputsout = outputs;
end
