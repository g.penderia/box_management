%Marine EUVRARD. Guillaume PENDERIA. 8/11/2016
% tirage initial de la r�gle de pr�sentation des formes sous forme de tableau � 1 ligne et 3 colonnes. A chaque forme
% (1,2 ou 3)est associ�e 1 ou 0. 1 signifie que la souris devra appuyer sur
% l'�cran de droite quand la forme associ�e sera pr�sent�e, sur l'�cran de
% gauche sinon. On d�finit une forme target (associ�e au toucher d'un �cran) et deux
% formes non target (associ�e au toucher de l'autre �cran)

function regle = tableau
    regle = [];
    %tirage au sort de la valeur des deux premi�res colonnes au hasard
    regle(end+1)=randi([0,1]);
    regle(end+1)=randi([0,1]);
    % d�termination de la 3�me valeur de la colonne en fonction des deux premi�res colonnes
    if(sum(regle)==2)
        regle(end+1) = 0;
    elseif(sum(regle)==0)
        regle(end+1) = 1;
    else
        regle(end+1)=randi([0,1]);
    end
end