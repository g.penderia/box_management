%Marine EUVRARD. 13/11/2016
% tirage de la nouvelle r�gle (association d'une forme � un c�t�) en
% fonction de l'ancienne r�gle car certaines ne peuvent pas s'encha�ner :
% il faut qu'il y ait au maximum un �l�ment en commun entre les deux r�gles


function regle = tableaunew3(previous)
%liste de toutes les r�gles qui peuvent s'encha�ner � une r�gle sous forme
%de matrice (1 r�gle par ligne)
A = [0,1,1; 1,0,1; 0,0,1];
B = [1,1,0;1,0,1;1,0,0];
C = [1,1,0; 0,1,1; 0,1,1];
D = [1,0,0; 0,1,0; 1,1,0];
E = [0,0,1; 0,1,0; 0,1,1];
F = [0,0,1; 1,0,0; 1,0,1];
a = randi([1,3])
%estimation de la valeur de la r�gle initiale, et au cas par cas choix
%d'une matrice contenant les r�gles possibles pour suivre la r�gle
%initiale, et tirage au sort d'une des lignes de la matrice pour constituer
%la nouvelle r�gle
if previous == [1,1,0]
    regle = A(a:a,1:3);
elseif previous == [0,1,1]
    regle = B(a:a,1:3);
elseif previous == [1,0,1]
    regle = C(a:a,1:3);
elseif previous == [0,0,1]
    regle = D(a:a,1:3);
elseif previous == [1,0,0]
    regle = E(a:a,1:3);
elseif previous == [0,1,0]
    regle = F(a:a,1:3);
end
end
