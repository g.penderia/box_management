
/***************************************************
  This is our touchscreen painting example for the Adafruit ILI9341 Breakout
  ----> http://www.adafruit.com/products/1770

  Check out the links above for our tutorials and wiring diagrams
  These displays use SPI to communicate, 4 or 5 pins are required to
  interface (RST is optional)
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution
 ****************************************************/

/** NOT FOR USE WITH THE TOUCH SHIELD, ONLY FOR THE BREAKOUT! **/

#include <Adafruit_GFX.h>    // Core graphics library
#include <SPI.h>
#include <Adafruit_ILI9341.h>
#include <Wire.h>      // this is needed for FT6206
#include <Adafruit_FT6206.h>

// The pin associated with the med output signal (from med to arduino)
const int fullscreen = A0;
const int stim = A1;
const int screenblink = A2;

// The pin associated with the med input signal when the screen is touched at the right time (from arduino to med)
const int touch = 1;
//const int led = 13;

// The FT6206 uses hardware I2C (SCL/SDA)
Adafruit_FT6206 ctp = Adafruit_FT6206();

// The display uses hardware SPI, plus #9 & #10
#define TFT_CS 5
#define TFT_DC 4
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

#define thickness 5
#define stimsize 150
int i, x, y;
int numTask = 0;
int touch_time;
int last_touch_time;

void setup(void) { 
  Serial.begin(115200);

  // Initiation of interrupt pin (the int.2 = 0)
  attachInterrupt(2, touchinterrupt, LOW);
  
  tft.begin();
  ctp.begin(10);
  tft.fillRect(0,0,240,320,0x0000);
  pinMode(stim, INPUT);
  pinMode(fullscreen, INPUT);
  pinMode(screenblink, INPUT);
  pinMode(touch, OUTPUT);

  while(numTask == 0) {
    int expected = 0;
    int nbPulses = 0;
    long initTime = millis();
    while(millis()-initTime<100){
      if(digitalRead(stim)==HIGH && expected==1){
        expected = 0;
      } else if (digitalRead(stim)==LOW && expected==0){
        nbPulses = nbPulses+1;
        expected = 1;
      }
    }
    if(nbPulses == 1) {
      numTask = 1;
    } else if (nbPulses == 2) {
      numTask = 2;
    } else if (nbPulses == 3) {
      numTask = 3;
    } else {
      numTask = 0;
    }
  }
}


void loop()
{
  if(numTask == 1) {
    flexibility();
  } else if (numTask == 2) {
    checking();
  } else if (numTask == 3) {
    switchTask();
  }
}

void touchinterrupt(){
  touch_time = millis();
  // check to see if touchinterrupt() was called in the last 200 milliseconds
  if (touch_time - last_touch_time > 200) {
    digitalWrite(touch, HIGH);
    delayMicroseconds(15000);
    delayMicroseconds(10000);
    delayMicroseconds(15000);
    delayMicroseconds(10000);
    delayMicroseconds(15000);
    delayMicroseconds(10000);
    delayMicroseconds(15000);
    delayMicroseconds(10000);
    digitalWrite(touch, LOW);
    touch_time = touch_time+100;
  }
  last_touch_time = touch_time;
}

////////////////////////////////////////////////////
/////////////// TACHE DE FLEXIBILITY ///////////////
////////////////////////////////////////////////////

void flexibility () {
  while (analogRead(fullscreen) < 100 && analogRead(stim) < 100 && analogRead(screenblink) > 100) {
    tft.fillRect(0,0,240,240,ILI9341_WHITE);
  }
  tft.fillRect(0,0,240,240,ILI9341_BLACK);
  if (analogRead(fullscreen) > 100 && analogRead(stim) > 100 && analogRead(screenblink) > 100) tft.fillRect(0,0,240,240,ILI9341_BLACK);
  if (analogRead(fullscreen) < 100 && analogRead(stim) > 100 && analogRead(screenblink) > 100) grid();
  tft.fillRect(0,0,240,240,ILI9341_BLACK);
  if (analogRead(fullscreen) > 100 && analogRead(stim) < 100 && analogRead(screenblink) > 100) line();
  tft.fillRect(0,0,240,240,ILI9341_BLACK);
  while (analogRead(screenblink) < 100 && analogRead(fullscreen) > 100 && analogRead(stim) > 100) {
    feedback();
  }
}

void line() {
  int space = 3;
  for (int x = 0; x <= 240; x = x + space + 6) {
    for (int xBis = x; xBis <= x + 6; xBis++) {
      tft.drawFastVLine(xBis, 0, 240, ILI9341_WHITE);
    }
    space = space + (space / 2);
  }
  for (;;) {
    if (analogRead(stim) > 100 || analogRead(screenblink) < 100) return;
  }
}

void grid() {
  for (int x = 2; x <= 240; x = x + 58) {
    if (x >= 235) {
      x = 236;
    }
    if (x < 3) {
      x = 3;
    }
    for (int ep = 0; ep <= 3; ep++) {
      tft.drawFastVLine(x + ep, 0, 240, ILI9341_WHITE);
      tft.drawFastVLine(x - ep, 0, 240, ILI9341_WHITE);
      tft.drawFastHLine(0, x + ep, 240, ILI9341_WHITE);
      tft.drawFastHLine(0, x - ep, 240, ILI9341_WHITE);
    }
  }
  for (;;) {
    if (analogRead(fullscreen) > 100 || analogRead(screenblink) < 100) return;
  }
}

void feedback() {
  tft.fillRect(40,0,160,240,ILI9341_WHITE);
  tft.fillRect(40,0,160,240,ILI9341_BLACK);
}

/////////////////////////////////////////////////
/////////////// TACHE DE CHECKING ///////////////
/////////////////////////////////////////////////

void checking () {
  if(digitalRead(stim)==LOW){
    float ratio = getRatioFromSignal();
    draw(ratio);
  }
  if(digitalRead(fullscreen)==LOW){
    fullScreenFcn();
  }
  if(digitalRead(screenblink)==LOW){
    feedback();
  }
}


float getRatioFromSignal() { 
  int expected = 1;
  int nbPulses = 1;
  long initTime = millis();
  float ratio;
  while(millis()-initTime<100){
    if(digitalRead(stim)==HIGH && expected==1){
      expected = 0;
    } else if (digitalRead(stim)==LOW && expected==0){
      nbPulses = nbPulses+1;
      expected = 1;
    }
  }
  /* This can be used to display the number of pulses received
  tft.setCursor(190, 170);
  tft.setTextColor(ILI9341_WHITE);
  tft.setTextSize(1);
  tft.println(nbPulses);*/
  ratio = float(nbPulses)/100;
  return ratio;
}

void draw(float ratio) { 
  // The commented parts of x and y are to be used if the stimulus must
  // be displayed at a random location on the screen
  x = 120; //random((stimsize/2)+thickness, 240-(stimsize/2)+thickness);
  y = 160; //random((stimsize/2)+thickness, 320-(stimsize/2)+thickness);
  tft.fillCircle(x, y, thickness, 0xFFFF);
  for(i=2;i<stimsize/2;i=i+2){
    if((float(i)/(float(stimsize)/2.0)) <= ratio){
        tft.fillCircle(x-i, y-i, thickness, 0xFFFF);
        tft.fillCircle(x+i, y+i, thickness, 0xFFFF);
    }/* else { //used to see the max size
      tft.fillCircle(x-i, y-i, thickness, ILI9341_RED);
      tft.fillCircle(x+i, y+i, thickness, ILI9341_RED);
    }*/
    if((float(i)/(float(stimsize)/2.0)) <= 1.0-ratio){
        tft.fillCircle(x-i, y+i, thickness, 0xFFFF);
        tft.fillCircle(x+i, y-i, thickness, 0xFFFF);
    }/* else { // used to see the max size
      tft.fillCircle(x-i, y+i, thickness, ILI9341_RED);
      tft.fillCircle(x+i, y-i, thickness, ILI9341_RED);
    }*/
  }
  while(digitalRead(stim)==LOW){
  }
  tft.fillRect(0,0,240,320,0x0000);
}

void fullScreenFcn(){
  tft.fillRect(0,0,240,320,0xFFFF);
  while(digitalRead(fullscreen)==LOW){
  }
  tft.fillRect(0,0,240,320,0x0000);
}

////////////////////////////////////////////////////
////////////////// TACHE DE SWITCH /////////////////
////////////////////////////////////////////////////

void switchTask () {
  if(digitalRead(stim)==LOW){
    float ratio = getRatioFromSignal();
    drawSwitch(ratio);
  }
  if(digitalRead(fullscreen)==LOW){
    fullScreenFcn();
  }
  if(digitalRead(screenblink)==LOW){
    feedback();
  }
}

void drawSwitch(float ratio) {
  if(ratio == 0.01){
    drawGrid();
  } else if(ratio == 0.02) {
    drawVerticalLines();
  } else if(ratio == 0.03) {
    drawDiagonalLines();
  }
}

void drawGrid() {
  for (int x = 2; x <= 240; x = x + 58) {
    if (x >= 235) {
      x = 236;
    }
    if (x < 3) {
      x = 3;
    }
    for (int ep = 0; ep <= 3; ep++) {
      tft.drawFastVLine(x + ep, 0, 240, 0xFFFF);
      tft.drawFastVLine(x - ep, 0, 240, 0xFFFF);
      tft.drawFastHLine(0, x + ep, 240, 0xFFFF);
      tft.drawFastHLine(0, x - ep, 240, 0xFFFF);
    }
  }
  while(digitalRead(stim)==LOW){
  }
  tft.fillRect(0,0,240,320,0x0000);
}

void drawVerticalLines() {
  int space = 3;
  for (int x = 0; x <= 240; x = x + space + 6) {
    for (int xBis = x; xBis <= x + 6; xBis++) {
      tft.drawFastVLine(xBis, 0, 240, 0xFFFF);
    }
    space = space + (space / 2);
  }
  while(digitalRead(stim)==LOW){
  }
  tft.fillRect(0,0,240,320,0x0000);
}


void drawDiagonalLines() {
  int y1 = 0; //random((stimsize/2)+thickness, 320-(stimsize/2)+thickness);
  int y2 = 125;
  for(i=0;i<35;i++){
    tft.drawLine(0,y1+i,239,y2+i,0xFFFF);
    tft.drawLine(239,319-(y1+i),0,195-(y2+i),0xFFFF);
  }
  /*int epaisseur = 35;
  tft.fillCircle(x, y, epaisseur, 0xFFFF);
  while(x<240) {
    tft.fillCircle(x, y, epaisseur, 0xFFFF);
    tft.fillCircle(239 - x, 319 - y, epaisseur, 0xFFFF);
    x = x + 2;
    y = y + 1;
  }*/
  while(digitalRead(stim)==LOW){
  }
  tft.fillRect(0,0,240,320,0x0000);
}
