function varargout = prepareBoxes(varargin)
% prepareBoxes MATLAB code for prepareBoxes.fig
%      prepareBoxes, by itself, creates a new prepareBoxes or raises the existing
%      singleton*.
%
%      H = prepareBoxes returns the handle to a new prepareBoxes or the handle to
%      the existing singleton*.
%
%      prepareBoxes('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in prepareBoxes.M with the given input arguments.
%
%      prepareBoxes('Property','Value',...) creates a new prepareBoxes or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before prepareBoxes_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to prepareBoxes_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2014 The MathWorks, Inc.

% Edit the above text to modify the response to help prepareBoxes

% Last Modified by GUIDE v2.5 08-Jul-2016 11:50:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @prepareBoxes_OpeningFcn, ...
                   'gui_OutputFcn',  @prepareBoxes_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before prepareBoxes is made visible.
function prepareBoxes_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to prepareBoxes (see VARARGIN)
handles.list = varargin{1};
handles.box = varargin{2};
handles.varargin = varargin{2};

handles.ok = 0;

% Update handles structure

% UIWAIT makes prepareBoxes wait for user response (see UIRESUME)
movegui(handles.prepareBoxes, 'center');


%Create tab group
handles.tgroup = uitabgroup('Parent', handles.prepareBoxes,'TabLocation', 'top','Position',[.0 .0 1 .915]);
if(ismember(1, handles.list))
    handles.tab_box1 = uitab('Parent', handles.tgroup, 'Title', 'Box 1');
    handles.box1_panel.BackgroundColor = [.871 .922 .98];
    set(handles.box1_panel,'Parent',handles.tab_box1);
else
    set(handles.box1_panel,'visible','off');
end

if(ismember(2, handles.list))
    handles.tab_box2 = uitab('Parent', handles.tgroup, 'Title', 'Box 2');
    handles.box2_panel.BackgroundColor = [.871 .922 .98];
    set(handles.box2_panel,'Parent',handles.tab_box2);
    set(handles.box2_panel,'position',get(handles.box1_panel,'position'));
end

if(ismember(3, handles.list))
    handles.tab_box3 = uitab('Parent', handles.tgroup, 'Title', 'Box 3');
    handles.box3_panel.BackgroundColor = [.871 .922 .98];
    set(handles.box3_panel,'Parent',handles.tab_box3);
    set(handles.box3_panel,'position',get(handles.box1_panel,'position'));
end

if(ismember(4, handles.list))
    handles.tab_box4 = uitab('Parent', handles.tgroup, 'Title', 'Box 4');
    handles.box4_panel.BackgroundColor = [.871 .922 .98];
    set(handles.box4_panel,'Parent',handles.tab_box4);
    set(handles.box4_panel,'position',get(handles.box1_panel,'position'));
end

guidata(hObject, handles);

if(isfield(handles.box(handles.list(1)), 'protocole'))
    vars = {'expname', 'expteam', 'animID', 'animweight', 'notes'};
    for i = handles.list
        for x = 1:length(vars)
            set(handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', vars{x}))), 'String', handles.box(i).(matlab.lang.makeValidName(vars{x})));
        end
        if(strcmp(handles.box(i).animgender, 'Female'))
            set(handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', 'animgender'))),'SelectedObject', handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', 'animfemale'))));
        else
            set(handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', 'animgender'))),'SelectedObject', handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', 'animmale'))));
        end
        if(strcmp(handles.box(i).protocole, 'Flexibility'))
            set(handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', 'protocole'))), 'Value', 2);
            set(handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', 'setVariables'))), 'enable', 'on');
        elseif(strcmp(handles.box(i).protocole, 'Switch'))
            set(handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', 'protocole'))), 'Value', 3);
            set(handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', 'setVariables'))), 'enable', 'on');
        else
            set(handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', 'protocole'))), 'Value', 1);
            set(handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', 'setVariables'))), 'enable', 'off');
        end
    end
end
        
uiwait(handles.prepareBoxes);

% --- Outputs from this function are returned to the command line.
function varargout = prepareBoxes_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
if(handles.ok)
    varargout{1} = handles.box;
else
    varargout{1} = handles.varargin;
end
delete(handles.prepareBoxes);



function prepareBoxes_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to launcher (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: delete(hObject) closes the figure
close=questdlg('Close without saving ?','Confirm your action','Yes','No','No');
if(strcmp(close,'Yes'))
    uiresume(handles.prepareBoxes);
end



function b1_notes_Callback(hObject, eventdata, handles)
% hObject    handle to b1_notes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b1_notes as text
%        str2double(get(hObject,'String')) returns contents of b1_notes as a double


% --- Executes during object creation, after setting all properties.
function b1_notes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b1_notes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in b1_protocole.
function b1_protocole_Callback(hObject, eventdata, handles)
% hObject    handle to b1_protocole (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns b1_protocole contents as cell array
%        contents{get(hObject,'Value')} returns selected item from b1_protocole
contents = get(handles.b1_protocole,'String');
if(isempty(contents{get(handles.b1_protocole,'Value')}))
    set(handles.b1_setVariables, 'enable','off');
else
    set(handles.b1_setVariables, 'enable','on');
end


% --- Executes during object creation, after setting all properties.
function b1_protocole_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b1_protocole (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in b1_recordPhy.
function b1_recordPhy_Callback(hObject, eventdata, handles)
% hObject    handle to b1_recordPhy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of b1_recordPhy


% --- Executes on button press in b1_recordVideo.
function b1_recordVideo_Callback(hObject, eventdata, handles)
% hObject    handle to b1_recordVideo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of b1_recordVideo


% --- Executes on button press in saveExit.
function saveExit_Callback(hObject, eventdata, handles)
% hObject    handle to saveExit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
vars = {'expname', 'expteam', 'animID', 'animweight', 'notes'};
for i = handles.list
    for x = 1:length(vars)
        handles.box(i).(matlab.lang.makeValidName(vars{x})) = get(handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', vars{x}))), 'String');
    end
    handles.box(i).animgender = get(get(handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', 'animgender'))),'SelectedObject'), 'String');
    contents = get(handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', 'protocole'))),'String');
	handles.box(i).protocole = contents{get(handles.(matlab.lang.makeValidName(strcat('b', sprintf('%d', i), '_', 'protocole'))),'Value')};
end
handles.ok = 1;

guidata(handles.prepareBoxes, handles);
uiresume(handles.prepareBoxes);



function b3_notes_Callback(hObject, eventdata, handles)
% hObject    handle to b3_notes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b3_notes as text
%        str2double(get(hObject,'String')) returns contents of b3_notes as a double


% --- Executes during object creation, after setting all properties.
function b3_notes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b3_notes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in b3_protocole.
function b3_protocole_Callback(hObject, eventdata, handles)
% hObject    handle to b3_protocole (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns b3_protocole contents as cell array
%        contents{get(hObject,'Value')} returns selected item from b3_protocole
contents = get(handles.b3_protocole,'String');
if(isempty(contents{get(handles.b3_protocole,'Value')}))
    set(handles.b3_setVariables, 'enable','off');
else
    set(handles.b3_setVariables, 'enable','on');
end

% --- Executes during object creation, after setting all properties.
function b3_protocole_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b3_protocole (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in b3_recordPhy.
function b3_recordPhy_Callback(hObject, eventdata, handles)
% hObject    handle to b3_recordPhy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of b3_recordPhy


% --- Executes on button press in b3_recordVideo.
function b3_recordVideo_Callback(hObject, eventdata, handles)
% hObject    handle to b3_recordVideo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of b3_recordVideo


% --- Executes on button press in b3_setVariables.
function b3_setVariables_Callback(hObject, eventdata, handles)
% hObject    handle to b3_setVariables (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
contents = get(handles.b3_protocole,'String');
switch contents{get(handles.b3_protocole,'Value')}
    case 'Flexibility'
        if(~isfield(handles.box(3), 'params'))
            handles.box(3).params = 0;
        end
        handles.box(3).params = setVariablesFlexibility(handles.box(3).params);
    case 'Switch'
        if(~isfield(handles.box(3), 'params'))
            handles.box(3).params = 0;
        end
        handles.box(3).params = setVariablesSwitch(handles.box(3).params);
end
guidata(handles.prepareBoxes, handles);


function b2_notes_Callback(hObject, eventdata, handles)
% hObject    handle to b2_notes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b2_notes as text
%        str2double(get(hObject,'String')) returns contents of b2_notes as a double


% --- Executes during object creation, after setting all properties.
function b2_notes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b2_notes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in b2_protocole.
function b2_protocole_Callback(hObject, eventdata, handles)
% hObject    handle to b2_protocole (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns b2_protocole contents as cell array
%        contents{get(hObject,'Value')} returns selected item from b2_protocole
contents = get(handles.b2_protocole,'String');
if(isempty(contents{get(handles.b2_protocole,'Value')}))
    set(handles.b2_setVariables, 'enable','off');
else
    set(handles.b2_setVariables, 'enable','on');
end

% --- Executes during object creation, after setting all properties.
function b2_protocole_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b2_protocole (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in b2_recordPhy.
function b2_recordPhy_Callback(hObject, eventdata, handles)
% hObject    handle to b2_recordPhy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of b2_recordPhy


% --- Executes on button press in b2_recordVideo.
function b2_recordVideo_Callback(hObject, eventdata, handles)
% hObject    handle to b2_recordVideo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of b2_recordVideo


% --- Executes on button press in b2_setVariables.
function b2_setVariables_Callback(hObject, eventdata, handles)
% hObject    handle to b2_setVariables (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
contents = get(handles.b2_protocole,'String');
switch contents{get(handles.b2_protocole,'Value')}
    case 'Flexibility'
        if(~isfield(handles.box(2), 'params'))
            handles.box(2).params = 0;
        end
        handles.box(2).params = setVariablesFlexibility(handles.box(2).params);
    case 'Switch'
        if(~isfield(handles.box(2), 'params'))
            handles.box(2).params = 0;
        end
        handles.box(2).params = setVariablesSwitch(handles.box(2).params);
end
guidata(handles.prepareBoxes, handles);

% --- Executes on button press in b4_setVariables.
function b4_setVariables_Callback(hObject, eventdata, handles)
% hObject    handle to b4_setVariables (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
contents = get(handles.b4_protocole,'String');
switch contents{get(handles.b4_protocole,'Value')}
    case 'Flexibility'
        if(~isfield(handles.box(4), 'params'))
            handles.box(4).params = 0;
        end
        handles.box(4).params = setVariablesFlexibility(handles.box(4).params);
    case 'Switch'
        if(~isfield(handles.box(4), 'params'))
            handles.box(4).params = 0;
        end
        handles.box(4).params = setVariablesSwitch(handles.box(4).params);
end
guidata(handles.prepareBoxes, handles);

% --- Executes on button press in b4_recordVideo.
function b4_recordVideo_Callback(hObject, eventdata, handles)
% hObject    handle to b4_recordVideo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of b4_recordVideo


% --- Executes on button press in b4_recordPhy.
function b4_recordPhy_Callback(hObject, eventdata, handles)
% hObject    handle to b4_recordPhy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of b4_recordPhy


% --- Executes on selection change in b4_protocole.
function b4_protocole_Callback(hObject, eventdata, handles)
% hObject    handle to b4_protocole (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns b4_protocole contents as cell array
%        contents{get(hObject,'Value')} returns selected item from b4_protocole
contents = get(handles.b4_protocole,'String');
if(isempty(contents{get(handles.b4_protocole,'Value')}))
    set(handles.b4_setVariables, 'enable','off');
else
    set(handles.b4_setVariables, 'enable','on');
end

% --- Executes during object creation, after setting all properties.
function b4_protocole_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b4_protocole (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b4_notes_Callback(hObject, eventdata, handles)
% hObject    handle to b4_notes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b4_notes as text
%        str2double(get(hObject,'String')) returns contents of b4_notes as a double


% --- Executes during object creation, after setting all properties.
function b4_notes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b4_notes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in b1_setVariables.
function b1_setVariables_Callback(hObject, eventdata, handles)
% hObject    handle to b1_setVariables (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
contents = get(handles.b1_protocole,'String');
switch contents{get(handles.b1_protocole,'Value')}
    case 'Flexibility'
        if(~isfield(handles.box(1), 'params'))
            handles.box(1).params = 0;
        end
        handles.box(1).params = setVariablesFlexibility(handles.box(1).params);
    case 'Switch'
        if(~isfield(handles.box(1), 'params'))
            handles.box(1).params = 0;
        end
        handles.box(1).params = setVariablesSwitch(handles.box(1).params);
end
guidata(handles.prepareBoxes, handles);


function b1_animID_Callback(hObject, eventdata, handles)
% hObject    handle to b1_animID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b1_animID as text
%        str2double(get(hObject,'String')) returns contents of b1_animID as a double


% --- Executes during object creation, after setting all properties.
function b1_animID_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b1_animID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b1_animweight_Callback(hObject, eventdata, handles)
% hObject    handle to b1_animweight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b1_animweight as text
%        str2double(get(hObject,'String')) returns contents of b1_animweight as a double


% --- Executes during object creation, after setting all properties.
function b1_animweight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b1_animweight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b1_expname_Callback(hObject, eventdata, handles)
% hObject    handle to b1_expname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b1_expname as text
%        str2double(get(hObject,'String')) returns contents of b1_expname as a double


% --- Executes during object creation, after setting all properties.
function b1_expname_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b1_expname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b1_expteam_Callback(hObject, eventdata, handles)
% hObject    handle to b1_expteam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b1_expteam as text
%        str2double(get(hObject,'String')) returns contents of b1_expteam as a double


% --- Executes during object creation, after setting all properties.
function b1_expteam_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b1_expteam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b4_expname_Callback(hObject, eventdata, handles)
% hObject    handle to b4_expname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b4_expname as text
%        str2double(get(hObject,'String')) returns contents of b4_expname as a double


% --- Executes during object creation, after setting all properties.
function b4_expname_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b4_expname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b4_expteam_Callback(hObject, eventdata, handles)
% hObject    handle to b4_expteam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b4_expteam as text
%        str2double(get(hObject,'String')) returns contents of b4_expteam as a double


% --- Executes during object creation, after setting all properties.
function b4_expteam_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b4_expteam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b4_animID_Callback(hObject, eventdata, handles)
% hObject    handle to b4_animID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b4_animID as text
%        str2double(get(hObject,'String')) returns contents of b4_animID as a double


% --- Executes during object creation, after setting all properties.
function b4_animID_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b4_animID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b4_animweight_Callback(hObject, eventdata, handles)
% hObject    handle to b4_animweight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b4_animweight as text
%        str2double(get(hObject,'String')) returns contents of b4_animweight as a double


% --- Executes during object creation, after setting all properties.
function b4_animweight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b4_animweight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b2_animID_Callback(hObject, eventdata, handles)
% hObject    handle to b2_animID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b2_animID as text
%        str2double(get(hObject,'String')) returns contents of b2_animID as a double


% --- Executes during object creation, after setting all properties.
function b2_animID_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b2_animID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b2_animweight_Callback(hObject, eventdata, handles)
% hObject    handle to b2_animweight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b2_animweight as text
%        str2double(get(hObject,'String')) returns contents of b2_animweight as a double


% --- Executes during object creation, after setting all properties.
function b2_animweight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b2_animweight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b2_expname_Callback(hObject, eventdata, handles)
% hObject    handle to b2_expname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b2_expname as text
%        str2double(get(hObject,'String')) returns contents of b2_expname as a double


% --- Executes during object creation, after setting all properties.
function b2_expname_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b2_expname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b2_expteam_Callback(hObject, eventdata, handles)
% hObject    handle to b2_expteam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b2_expteam as text
%        str2double(get(hObject,'String')) returns contents of b2_expteam as a double


% --- Executes during object creation, after setting all properties.
function b2_expteam_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b2_expteam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b3_animID_Callback(hObject, eventdata, handles)
% hObject    handle to b3_animID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b3_animID as text
%        str2double(get(hObject,'String')) returns contents of b3_animID as a double


% --- Executes during object creation, after setting all properties.
function b3_animID_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b3_animID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b3_animweight_Callback(hObject, eventdata, handles)
% hObject    handle to b3_animweight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b3_animweight as text
%        str2double(get(hObject,'String')) returns contents of b3_animweight as a double


% --- Executes during object creation, after setting all properties.
function b3_animweight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b3_animweight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b3_expname_Callback(hObject, eventdata, handles)
% hObject    handle to b3_expname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b3_expname as text
%        str2double(get(hObject,'String')) returns contents of b3_expname as a double


% --- Executes during object creation, after setting all properties.
function b3_expname_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b3_expname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function b3_expteam_Callback(hObject, eventdata, handles)
% hObject    handle to b3_expteam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of b3_expteam as text
%        str2double(get(hObject,'String')) returns contents of b3_expteam as a double


% --- Executes during object creation, after setting all properties.
function b3_expteam_CreateFcn(hObject, eventdata, handles)
% hObject    handle to b3_expteam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
