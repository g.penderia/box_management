function varargout = launcher(varargin)
% LAUNCHER MATLAB code for launcher.fig
%      LAUNCHER, by itself, creates a new LAUNCHER or raises the existing
%      singleton*.
%
%      H = LAUNCHER returns the handle to a new LAUNCHER or the handle to
%      the existing singleton*.
%
%      LAUNCHER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LAUNCHER.M with the given input arguments.
%
%      LAUNCHER('Property','Value',...) creates a new LAUNCHER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before launcher_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to launcher_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help launcher

% Last Modified by GUIDE v2.5 01-Jul-2016 12:24:02

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @launcher_OpeningFcn, ...
                   'gui_OutputFcn',  @launcher_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before launcher is made visible.
function launcher_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to launcher (see VARARGIN)

% Choose default command line output for launcher
handles.output = hObject;

% Update handles structure
handles.boxes = 0;
guidata(hObject, handles);
movegui(handles.launcher, 'center');

% UIWAIT makes launcher wait for user response (see UIRESUME)
% uiwait(handles.launcher);


% --- Outputs from this function are returned to the command line.
function varargout = launcher_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in prepareBoxesButton.
function prepareBoxesButton_Callback(hObject, eventdata, handles)
% hObject    handle to prepareBoxesButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
list = [];
if(get(handles.checkbox1, 'Value') > 0)
    list(end+1) = 1;
end
if(get(handles.checkbox2, 'Value') > 0)
    list(end+1) = 2;
end
if(get(handles.checkbox3, 'Value') > 0)
    list(end+1) = 3;
end
if(get(handles.checkbox4, 'Value') > 0)
    list(end+1) = 4;
end
if(~isempty(list))
    set(handles.launcher, 'Visible', 'Off');
    handles.boxes=prepareBoxes(list, handles.boxes);
    set(handles.launcher, 'Visible', 'On');
    if(isfield(handles.boxes(list(1)), 'params'))
        if(isstruct(handles.boxes(list(1)).params))
            set(handles.launchButton,'enable','on');
        end
    end
    handles.list = list;
    guidata(handles.launcher, handles);
else
    warndlg('You must select a box first.','Error');
end

% --- Executes on button press in launchButton.
function launchButton_Callback(hObject, eventdata, handles)
% hObject    handle to launchButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
list = [];
if(get(handles.checkbox1, 'Value') > 0)
    list(end+1) = 1;
end
if(get(handles.checkbox2, 'Value') > 0)
    list(end+1) = 2;
end
if(get(handles.checkbox3, 'Value') > 0)
    list(end+1) = 3;
end
if(get(handles.checkbox4, 'Value') > 0)
    list(end+1) = 4;
end
if(isempty(list))
    warndlg('You must select a box first.','Error');
elseif(any(list ~= handles.list))
    warndlg('Box selection must not be changed after parameters are set.');
else
    luminance(handles.boxes, list);
    delete(handles.launcher);
end


% --- Executes on button press in wiringConfigButton.
function wiringConfigButton_Callback(hObject, eventdata, handles)
% hObject    handle to wiringConfigButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.launcher,'Visible','off');
handles.boxes = wiringConfig(handles.boxes);
set(handles.launcher,'Visible','on');
if(~isstruct(handles.boxes))
    set(handles.prepareBoxesButton,'enable','off');
else
    set(handles.prepareBoxesButton,'enable','on');
end
guidata(handles.launcher, handles);



function launcher_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to launcher (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: delete(hObject) closes the figure
close=questdlg('Are you sure you want to close the program ?','Confirm your action','Yes','No','No');
if(strcmp(close,'Yes'))
    delete(handles.launcher);
end
