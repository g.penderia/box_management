% Luminance v2
% vMedState : Nabil Benzina
% v1 : Ma�va Gacoin, 2015
% v2 : Guillaume Penderia, 2016
%
% This file contains the main script of the box management program.
% It sequentially checks the state of each box, and works with this data
% according to the script.
%
% Called by : monitoring.m
% Calls : -
%
% Note :
% - tous les inputs devront � terme �tre enregistr�s

% Requis : https://github.com/Psychtoolbox-3/Psychtoolbox-3/releases/tag/PTB_Beta-2014-04-06_V3.0.11

function luminance(box, list)
    % Initialization of general parameters
    %sessionStart = datetime;
    outputs = prepareoutputs(box, list);
    inputs = prepareinputs(box, list);
    keystop = KbName('ESC');
    pathdata = 'D:\Mice_Behavioral_Data\Switch\';
    
    % Initialization of the boxes
    for i = list
        box(i).nosepoke.active = 0; % 0 = No // 1 = Yes
        box(i).rightTouch.active = 0; % 0 = No // 1 = Yes
        box(i).leftTouch.active = 0; % 0 = No // 1 = Yes
        box(i).redlight.active = 0; % 0 = No // 1 = Yes
        box(i).Stage = 'Start'; % Le premier est toujours 'Start'
        box(i).Substage = 0;
       % box(i).Tic = 0;
        box(i).totalPelletsRetrieved = 0;
        box(i).pelletsRetrievedPostEnd = 0;
        box(i).RepeatCount = 0;
        box(i).TrialNumber = 0;
        box(i).Data = [];
        box(i).Data.stage = [];
        box(i).Data.timestamp = [];
        box(i).Data.correctSide = []; % 0 = Left // 1 = Right
        box(i).Data.mouseAnswer = []; % 0 = Left // 1 = Right
        box(i).Data.correct = []; % 0 = No // 1 = Yes
        box(i).Data.answerTime = []; % 0 = no answer
        box(i).Data.rewardTaken = []; % 0 = No // 1 = Yes
        if(strcmp(box(i).protocole, 'Flexibility'))
            box(i).pelletsRetrievedStage1 = 0;
            box(i).pelletsRetrievedStage2 = 0;
            box(i).pelletsRetrievedStage3 = 0;
            box(i).pelletsRetrievedStage4 = 0;
            box(i).pelletsRetrievedStage5 = 0;
            box(i).TrialsStage4 = zeros(1, box(i).params.nbTrialsCalc);
            box(i).TrialsStage5 = zeros(1, box(i).params.nbTrialsCalc);
            box(i).TrialsStage6 = zeros(1, box(i).params.nbTrialsCalc);
            box(i).TrialsStage7 = zeros(1, box(i).params.nbTrialsCalc);
            box(i).TrialsStage8 = zeros(1, box(i).params.nbTrialsCalc);
            box(i).TrialsStage9 = zeros(1, box(i).params.nbTrialsCalc);
            box(i).TrialsStage10 = zeros(1, box(i).params.nbTrialsCalc);
            box(i).PreviousScreensOn = [0,1,0,1,0,1,0,1,0,1];
            box(i).nextScreen = [];
            box(i).Data.correctStim = [];
            box(i).Data.correctiveTrial = []; % 0 = No // 1 = Yes
        elseif(strcmp(box(i).protocole, 'Switch'))
            box(i).pelletsRetrievedStage1 = 0;
            box(i).pelletsRetrievedStage2 = 0;
            box(i).pelletsRetrievedStage3 = 0;
            box(i).pelletsRetrievedStage4 = 0;
            box(i).pelletsRetrievedStage5 = 0;
            box(i).TrialsStage4 = zeros(1, box(i).params.nbTrialsCalc);
            box(i).TrialsStage5 = zeros(1, box(i).params.nbTrialsCalc);
            box(i).TrialsStage6 = zeros(1, box(i).params.nbTrialsCalc);
            box(i).TrialsStage7 = zeros(1, box(i).params.nbTrialsCalc);
            box(i).TrialsStage8 = zeros(1, box(i).params.nbTrialsCalc);
            box(i).TrialsStage9 = zeros(1, box(i).params.nbTrialsCalc);
            box(i).TrialsStage10 = zeros(1, box(i).params.nbTrialsCalc);
            box(i).PreviousScreensOn = [0,1,0,1,0,1,0,1,0,1];
            box(i).nextStim = [];
            box(i).Data.correctStim = [];
            box(i).Data.correctiveTrial = []; % 0 = No // 1 = Yes
        end
    end
    
    % Initialization of the reading/writing libraries for inputs/outputs
    mex -setup;
    hfile =(fullfile(matlabroot, 'lib','win32', '704IO.h'));
    cppfile = (fullfile(matlabroot, 'lib', 'win32', 'Mex', 'Test704.cpp'));
    libfile = (fullfile(matlabroot, 'lib', 'win32', '704IO.lib'));
    if ~libisloaded('MedLibrary')
        % Allows writing with PortWrite
        [~,~]=loadlibrary('704IO',hfile,'alias','MedLibrary');
    end
    % Allows reading with PortRead
    mex('-g', cppfile, libfile);
    
    % Main loop
    %name = strcat('runTimes_', datestr(now, 'dd-mmm-YYYY HH-MM SS.FFF'), '.txt');
    %runTimeFile = fopen(name,'wt');
    %fprintf(runTimeFile, 'START : %s \n', datestr(now, 'dd-mmm-YYYY HH-MM SS.FFF'));
    finish = 0;
    namefile = strcat('data_', datestr(now, 'dd-mmm-YYYY HH-MM SS.FFF'),'.mat');
    namefile2 = strcat('data_', datestr(now, 'dd-mmm-YYYY HH-MM SS.FFF'),'_Backup.mat');
    trialsData = fopen(strcat(pathdata,strcat('data_', datestr(now, 'dd-mmm-YYYY HH-MM SS.FFF'),'.csv')),'wt');
    fprintf(trialsData, strcat('Box number,Timestamp,Mouse number,Trial number,Stage number,Success rate,Correctness (1:Success 0:Failure),Type of response (1:Correct 0:No answer -1:Wrong -2:pellet not retrieved),Response time,Pellet recovery time,Corrective trial,Stimulus ratio,Correct side,Mouse answer\n'));
    
    while(finish<length(list))
        %tic;
        finish = 0;
        box = getState(box, list, inputs); % Getting the readings of each box
        if(CheckKeyPress(keystop))
            for i = list 
                box(i).Stage = 'Finish';
            end
        end
        if(mod(minute(now),5) == 0) % Every five minutes (base on the clock not the start of the program)
            %save(strcat(pathdata, namefile, 'box'));
            save(strcat(pathdata, namefile));
            save(strcat(pathdata, namefile2));
        end
        for i = list % Processing according to each reading
            if (hour(now) <= 8 || hour(now) >= 20) && (box(i).redlight.active == 0)
                outputs = outputmanagement(outputs, box(i).redlight, 1); %Output On
                box(i).redlight.active = 1;
                disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Redlight activated.')); %STATEINFO
                
            
            elseif (hour(now) > 8  && hour(now) < 20) && (box(i).redlight.active == 1)
                    outputs = outputmanagement(outputs, box(i).redlight, -1); %Output Off
                    box(i).redlight.active = 0;
                    disp(strcat(datestr(now, 'dd-mmm-YYYY HH:MM:SS.FFF'), {' '}, 'Box ', int2str(i), ' - Redlight deactivated.')); %STATEINFO
                
            end
            %end
            if(strcmp(box(i).protocole, 'Flexibility'))
                [box, outputs] = flexibilityTask(box, i, outputs, trialsData);
            elseif(strcmp(box(i).protocole, 'Switch'))
                [box, outputs] = switchTask(box, i, outputs);
            end
            if(strcmp(box(i).Stage,'Finish'))
                finish = finish+1;
            end
        end
        %runTime=toc;
        %fprintf(runTimeFile, '%f\n', runTime);
    end
    %fclose(runTimeFile);
    %save(namefile, 'box');
    disp('END OF THE PROGRAM');
end





