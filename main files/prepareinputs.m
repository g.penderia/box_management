function inputs = prepareinputs(box, list)
    inputs = [];
    inputs.coordinates = [];
    %inputs.coordinates(1,1:3) = [0,0,0];
    nb = 0;
    for i = list
        if(strcmp(box(i).protocole, 'Flexibility'))
            for j = 1:3
                switch j
                    case 1
                        val = 'nosepoke';
                    case 2
                        val = 'leftTouch';
                    case 3
                        val = 'rightTouch';
                end
                add = 1;
                rack = box(i).(val).rack;
                port = box(i).(val).port;
                offset = box(i).(val).offset;
                for k = 1:nb
                    if(inputs.coordinates(k,1) == rack && inputs.coordinates(k,2) == port && inputs.coordinates(k,3) == offset)
                        add = 0;
                        break;
                    end
                end
                if(add)
                    inputs.coordinates(end+1,1:3) = [rack, port, offset];
                    nb = nb+1;
                end
            end
        elseif(strcmp(box(i).protocole, 'Switch'))
            for j = 1:3
                switch j
                    case 1
                        val = 'nosepoke';
                    case 2
                        val = 'leftTouch';
                    case 3
                        val = 'rightTouch';
                end
                add = 1;
                rack = box(i).(val).rack;
                port = box(i).(val).port;
                offset = box(i).(val).offset;
                for k = 1:nb
                    if(inputs.coordinates(k,1) == rack && inputs.coordinates(k,2) == port && inputs.coordinates(k,3) == offset)
                        add = 0;
                        break;
                    end
                end
                if(add)
                    inputs.coordinates(end+1,1:3) = [rack, port, offset];
                    nb = nb+1;
                end
            end
        end
    end
end