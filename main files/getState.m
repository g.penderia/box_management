% This function gathers the state of each box
function b = getState(box, list, inputs)
    [nb,~] = size(inputs.coordinates);
    for i = 1:nb
        rack = strcat('rack', int2str(inputs.coordinates(i, 1)));
        port = strcat('port', int2str(inputs.coordinates(i, 2)));
        offset = strrep(strcat('offset', int2str(inputs.coordinates(i, 3))), '-', 'm');
        inputs.(rack).(port).(offset)= double(Test704(inputs.coordinates(i, 1), inputs.coordinates(i, 2), inputs.coordinates(i, 3)));
    end
    for i = list
        for j = 1:3
            switch j
            case 1
                val = 'nosepoke';
            case 2
                val = 'leftTouch';
            case 3
                val = 'rightTouch';
            end
            rack = strcat('rack', int2str(box(i).(val).rack));
            port = strcat('port', int2str(box(i).(val).port));
            offset = strrep(strcat('offset', int2str(box(i).(val).offset)), '-', 'm');
            valueRead = inputs.(rack).(port).(offset);
            box(i).(val).active = active(box(i).(val).value, valueRead);
        end
    end
    b = box;
end