function outputsout = outputmanagement(outputsin, output, sign)
    rack = strcat('rack', int2str(output.rack));
    port = strcat('port', int2str(output.port));
    offset = strcat('offset', int2str(output.offset));
    val = outputsin.((rack)).((port)).((offset));
    if(sign >= 1)
        valOn = val + output.value;
        valOff = val;
        outputsin.((rack)).((port)).((offset)) = valOn;
        sign = sign - 1;
        for i = 1:sign
            calllib('MedLibrary','PortWrite',output.rack,output.port,output.offset,valOn);
            for i = 1:50 % boucle qui ralentit tr�s l�g�rement le programme afin que les signaux soient distinctibles
                x=now-i;
            end
            calllib('MedLibrary','PortWrite',output.rack,output.port,output.offset,valOff);
            for i = 1:50
                x=now-i;
            end
        end
        calllib('MedLibrary','PortWrite',output.rack,output.port,output.offset,valOn);
    elseif(sign == -1) %Normal outputs
        val = val + output.value*sign;
        if(val < 0)
            val = 0;
        end
        outputsin.((rack)).((port)).((offset)) = val;
        calllib('MedLibrary','PortWrite',output.rack,output.port,output.offset,val);
    else %Stimuli output
        valOn = val + output.value;
        valOff = val;
        ratio = sign * 100;
%         if(randi([0 1])) 
%             ratio = 100 - ratio;
%         end
        if(ratio == 0)
            ratio = 1;
        end
        ratio = ratio - 1;
        for i = 1:ratio
            calllib('MedLibrary','PortWrite',output.rack,output.port,output.offset,valOn);
            for i = 1:50 % boucle qui ralentit tr�s l�g�rement le programme afin que les signaux soient distinctibles
                x=now-i;
            end
            calllib('MedLibrary','PortWrite',output.rack,output.port,output.offset,valOff);
            for i = 1:50
                x=now-i;
            end
        end
        calllib('MedLibrary','PortWrite',output.rack,output.port,output.offset,valOn);
        outputsin.((rack)).((port)).((offset)) = valOn;
    end
    outputsout = outputsin;
end