function outputs = prepareoutputs(box, list)
    for i = list
        if(strcmp(box(i).protocole, 'Flexibility'))
            for j = 1:8
                switch j
                    case 1
                        val = 'reward';
                    case 2
                        val = 'blink';
                    case 3
                        val = 'leftScreenfig1';
                    case 4
                        val = 'leftScreenfig2';
                    case 5
                        val = 'rightScreenfig1';
                    case 6
                        val = 'rightScreenfig2';
                    case 7
                        val = 'light';
                    case 8
                        val = 'redlight';
                end
                rack = strcat('rack', int2str(box(i).(val).rack));
                port = strcat('port', int2str(box(i).(val).port));
                offset = strcat('offset', int2str(box(i).(val).offset));
                outputs.(rack).(port).(offset) = 0;
            end
        elseif (strcmp(box(i).protocole, 'Switch'))
            for j = 1:8
                switch j
                    case 1
                        val = 'reward';
                    case 2
                        val = 'blink';
                    case 3
                        val = 'leftScreenfig1';
                    case 4
                        val = 'leftScreenfig2';
                    case 5
                        val = 'rightScreenfig1';
                    case 6
                        val = 'rightScreenfig2';
                    case 7
                        val = 'light';
                    case 8
                        val = 'redlight';
                end
                rack = strcat('rack', int2str(box(i).(val).rack));
                port = strcat('port', int2str(box(i).(val).port));
                offset = strcat('offset', int2str(box(i).(val).offset));
                outputs.(rack).(port).(offset) = 0;
            end
        end
    end
end