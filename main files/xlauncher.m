% Launcher
% Guillaume Penderia, 2016
%
% This file is the launcher of the box management program.
% It gathers the necessary data from the user, and calls the other scripts.
%
% Called by : -
% Calls : luminance.m (Parameters : number of boxes, boxes)

function xlauncher
    disp('This launcher is only designed for tests, not the actual program.');
    %number_of_boxes=3;
    boxes(1).RackRead = 1;
    boxes(1).PortRead = 780;
    boxes(1).OffsetRead = -1;
    boxes(1).RackWrite = 1;
    boxes(1).PortWrite = 792;
    boxes(1).OffsetWrite = 0;
%     boxes(2).RackRead = 2;
%     boxes(2).PortRead = 780;
%     boxes(2).OffsetRead = -1;
%     boxes(2).RackWrite = 2;
%     boxes(2).PortWrite = 792;
%     boxes(2).OffsetRead = 0;
%     boxes(3).RackRead = 3;
%     boxes(3).PortRead = 780;
%     boxes(3).OffsetRead = -1;
%     boxes(3).RackWrite = 3;
%     boxes(3).PortWrite = 792;
%     boxes(3).OffsetRead = 0;
    
    trialsWindowSize = 40;
    successCutoff = 0.8;
    
    luminance(boxes, trialsWindowSize, successCutoff);
end