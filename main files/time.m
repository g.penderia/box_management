function timestr = time(d)
    d = datevec(d); %d must be a duration (datetime - datetime)
    timestr = strcat(num2str(d(3)),{' days, '},num2str(d(4)),{' hours, '},num2str(d(5)),{'min, '},num2str(d(6)),'s');
end