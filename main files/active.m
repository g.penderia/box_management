% This function tells if the input is active based on the value read
function var = active(inputValue, valueRead)
    inputBits = de2bi(inputValue,8);
    readBits = de2bi(valueRead,8);
    var = 0;
    for i = 1:8
        if(inputBits(i) == 1 && readBits(i) == 1)
            var = 1;
            break;
        end
    end
end